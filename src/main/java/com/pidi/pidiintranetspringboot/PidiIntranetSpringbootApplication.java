package com.pidi.pidiintranetspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PidiIntranetSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PidiIntranetSpringbootApplication.class, args);
	}

}
