package com.pidi.pidiintranetspringboot.controlleur;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pidi.pidiintranetspringboot.models.Fichier_PJ;
import com.pidi.pidiintranetspringboot.repository.Fichier_PJ_Repo;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/fichier_pj")

public class Fichier_PJ_Controlleur {

    @Autowired
    Fichier_PJ_Repo fichier_pj_Repository;

    @PostMapping("/upload")
    public Fichier_PJ uplaodImage(@RequestParam("myFile") MultipartFile file) throws IOException {

    	Fichier_PJ img = new Fichier_PJ( file.getOriginalFilename(),file.getContentType(),file.getBytes() );


        final Fichier_PJ savedImage = fichier_pj_Repository.save(img);


        System.out.println("Image saved");


        return savedImage;


    }
}