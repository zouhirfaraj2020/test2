package com.pidi.pidiintranetspringboot.controlleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pidi.pidiintranetspringboot.repository.GDP_ATT_MONTANT_REPO;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/GDP_ATT_MONTANT")
public class GDP_ATT_MONTANT_CONTROLLEUR {
	@Autowired
	private GDP_ATT_MONTANT_REPO GDP_repo;
	
	@GetMapping("/getRejetGdp")
	public List<Object[]> getRejetGdp() {
		return GDP_repo.findRejetGdp();
	}
	
	@GetMapping("/getRejetRqf")
	public List<Object[]> getRejetRqf() {
		return GDP_repo.findRejetRqf();
	}
	
	@GetMapping("/getValidationGdp") 
	public List<Object[]> getValidationGdp() {
		return GDP_repo.findValidationGdp();
	}
	
	@GetMapping("/getAttenteRqf")
	public List<Object[]> getAttenteRqf() {
		return GDP_repo.findAttenteRqf();
	}
	
	@GetMapping("/getAttenteGdp")
	public List<Object[]> getAttenteGdp() {
		return GDP_repo.findAttenteGdp();
	}
	
	@GetMapping("/getAttenteDt")
	public List<Object[]> getAttenteDt() {
		return GDP_repo.findAttenteDt();
	}
}
