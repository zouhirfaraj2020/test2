package com.pidi.pidiintranetspringboot.controlleur;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pidi.pidiintranetspringboot.models.ETAT_INTERVENTION;
import com.pidi.pidiintranetspringboot.repository.ETAT_INTERVENTION_REPO;
import com.pidi.pidiintranetspringboot.repository.GDP_ATT_MONTANT_REPO;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/ETAT_INTERVENTION")
public class ETAT_INTERVENTION_CONTROLLEUR {


	@Autowired
	private ETAT_INTERVENTION_REPO ETAT_INTERVENTION_repo;
	
	@GetMapping("/findAll")
	public List<ETAT_INTERVENTION> getDepts() {
		return ETAT_INTERVENTION_repo.findAll();
	}
}
