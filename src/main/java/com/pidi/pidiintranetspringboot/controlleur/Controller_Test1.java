


package com.pidi.pidiintranetspringboot.controlleur;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.exceptions.CsvValidationException;
import com.pidi.pidiintranetspringboot.models.Test1;
import com.pidi.pidiintranetspringboot.models.User;
import com.pidi.pidiintranetspringboot.repository.RepoTest1;
import com.pidi.pidiintranetspringboot.services.DBFileStorageService;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/test1")
public class Controller_Test1 {

	@Autowired
	private DBFileStorageService dbService;
	@Autowired
	private RepoTest1 repoTest1;
	
	@GetMapping("/findall")
	public List<Test1> getNotes() {
		return repoTest1.findAll();
	}

	@PostMapping("/add")
	public Test1 createNote(@RequestBody Test1 cpu) {
		return repoTest1.save(cpu);
	}
	
	/*@RequestMapping("/Test1")
	public void setDataInDb() {
		 dbService.saveDb();
	}*/
	
	/* List<String> files = new ArrayList<String>();
	   private final Path rootLocation = Paths.get("_Path_To_Save_The_File");

	   @PostMapping("/savefile")
	   public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file) {
	      String message;
	      try {
	         try {
	            Files.copy(file.getInputStream(), this.rootLocation.resolve("file_name.pdf"));
	         } catch (Exception e) {
	            throw new RuntimeException("FAIL!");
	         }
	         files.add(file.getOriginalFilename());

	         message = "Successfully uploaded!";
	         return ResponseEntity.status(HttpStatus.OK).body(message);
	      } catch (Exception e) {
	         message = "Failed to upload!";
	         return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
	      }
	   }*/
	
     /*  @PostMapping("/upload")
       public void addRfp  (@RequestParam("file") MultipartFile file) throws IOException{   
               //ZipSecureFile.setMinInflateRatio(0);
    	   ByteArrayInputStream f =  (ByteArrayInputStream) file.getInputStream();
               XSSFWorkbook workbook = new XSSFWorkbook(f); 
               XSSFSheet sheet = workbook.getSheetAt(0); 
               Row row;
               for(int i=1; i<=sheet.getLastRowNum(); i++){ 
 
                    row = (Row) sheet.getRow(i);
                    String id = "hello";
                       if( row.getCell(0).equals(null)) 
                       { System.out.print("fin"+id);}
                       else {
                           Test1 rfp = new Test1();
                           rfp.setId((int) row.getCell(0).getNumericCellValue());
                           rfp.setNom(row.getCell(1).toString());
                         
                            noteRepository.save(rfp);
                        }
                      }
       }*/
	
	

    /*@PostMapping("/upload-csv-file")
    public String uploadCSVFile(@RequestParam("file") MultipartFile file, Model model) {

        // validate file
        if (file.isEmpty()) {
            model.addAttribute("message", "Please select a CSV file to upload.");
            model.addAttribute("status", false);
        } else {

            // parse CSV file to create a list of `User` objects
            try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {

                // create csv bean reader
            	CsvToBean<Test1> csvToBean = new CsvToBeanBuilder<Test1>(reader)
                        .withType(Test1.class)
                        .withIgnoreLeadingWhiteSpace(true)
                        .build();
               

                // convert `CsvToBean` object to list of users
                List<Test1> tests = csvToBean.parse();
               
                // TODO: save users in DB?
                
                
                System.out.println(tests);
                for (Test1 var : tests) 
            	{ 
                	Test1 t =new Test1();
            	    t.setNom("aaaaa");
            	    t.setPrenom(var.getPrenom());
            	    createNote2(t);
            	    System.out.println("cest tttt:"+t);
            	}
                // save users list on model
                model.addAttribute("tests", tests);
                model.addAttribute("status", true);

            } catch (Exception ex) {
                model.addAttribute("message", "An error occurred while processing the CSV file.");
                model.addAttribute("status", false);
            }
        }

        return "file-upload-status";
    }
	
    
    public Test1 createNote2( Test1 cpu) {
		return repoTest1.save(cpu);
	}
    
    
  /*  @GetMapping("/importcsv")
    public List<Test1> getNotes2() {
    	String csvFile="src/main/resources/test.csv";
    	
    	CSVReader reader=null;
    	try {
    		reader=new CSVReader(new FileReader(csvFile));
    		String[] line;
    		
    		try {
				while((line = reader.readNext())!=null)
				{
				System.out.println("Test1 [nom="+line[0]+", prenom= "+line[1]);
				Test1 t=new Test1();
				t.setNom(line[0]);
				t.setPrenom(line[1]);
				System.out.println(repoTest1.save(t));
				}
			} catch (CsvValidationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	catch(IOException e)
    	{
    		e.printStackTrace();
    	}     	
		return repoTest1.findAll();
	}*/
    
    
 }
