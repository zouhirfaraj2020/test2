package com.pidi.pidiintranetspringboot.controlleur;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pidi.pidiintranetspringboot.models.Fichier_PJ;
import com.pidi.pidiintranetspringboot.models.PIECE_JOINTE;
import com.pidi.pidiintranetspringboot.repository.GDP_ATT_MONTANT_REPO;
import com.pidi.pidiintranetspringboot.repository.PIECE_JOINTE_REPO;
//import com.test.model.Note;
//import com.test.model.Note;
//import com.test.model.Departement;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/pj")
public class PIECE_JOINTE_CONTROLLEUR {

	@Autowired
	private PIECE_JOINTE_REPO PJ_repo;
	
	@PostMapping("/add")
	public PIECE_JOINTE createNote(
			@RequestParam(value ="ot_ID") Integer id_ot, 
			@RequestParam(value ="pjo_COMMENTAIRE") String comment,
			@RequestParam(value ="type_PJ_ID") String typePj) {
		PIECE_JOINTE pj = new PIECE_JOINTE();
		pj.setOT_ID(id_ot);
		pj.setPJO_COMMENTAIRE(comment);
	//String tp="gg";
		pj.setTYPE_PJ_ID(typePj);
		System.out.println(pj);
		return PJ_repo.save(pj);
	}
	@GetMapping("/add")
	public String createNote2(
			/*@ModelAttribute(value ="ot_ID") Integer id_ot, 
	        @ModelAttribute(value ="pjo_COMMENTAIRE") String comment,*/
		   @RequestParam(value ="type_PJ_ID") String typePj) {
	
		return "hhhhhh---------->"+typePj;
	}
	
	 @PostMapping("/upload")
	    public PIECE_JOINTE uplaodImage(@RequestParam("typo") String typePj,@RequestParam("myFile") MultipartFile file,@RequestParam(value ="pjo_COMMENTAIRE") String comment,@RequestParam("ot_ID") Integer ot_id) throws IOException {

	    	//Fichier_PJ img = new Fichier_PJ( file.getOriginalFilename(),file.getContentType(),file.getBytes() );
		 LocalDate localDate = LocalDate.now();
		 Date date = new Date(System.currentTimeMillis());
	    	PIECE_JOINTE pj = new PIECE_JOINTE();
	    	pj.setPJO_NOM(file.getOriginalFilename());
	    	pj.setTYPE_PJ_ID(typePj);
	    	pj.setPJO_COMMENTAIRE(comment);
	    	pj.setOT_ID(ot_id);
	    	pj.setPJO_DATE_INSERTION(date);


	        System.out.println("haddiiiii lovcal date "+date);


	        return PJ_repo.save(pj);

	    }
	
	@GetMapping("/findall")
	public List<PIECE_JOINTE> getNotes() {
		return PJ_repo.findAll();
	}
	
	@GetMapping("/findone/{id}")
	public List<PIECE_JOINTE> getPj(@PathVariable Long id) {
		return PJ_repo.findByOt(id);
	}
	
	@GetMapping("/findoneCri/{id}")
	public List<PIECE_JOINTE> getPjCri(@PathVariable Long id) {
		return PJ_repo.findByOtCri(id);
	}
	
	
	@GetMapping("/findoneCri2")
	public List<String> getPjCri2() {
		//return PJ_repo.findByOtCri(id);
		/*try (Stream<Path> walk = Files.walk(Paths.get("C:\\projects"))) {

			List<String> result = walk.filter(Files::isRegularFile)
					.map(x -> x.toString()).collect(Collectors.toList());

			result.forEach(System.out::println);

		} catch (IOException e) {
			e.printStackTrace();
		}*/
		List<String> result2=new ArrayList<String>();
		 String res = "";
		try (Stream<Path> walk = Files.walk(Paths.get("B:\\stage Atos\\stage\\pidi-intranet\\pidi-intranet-client-master\\src\\assets\\fichiers\\expo cri bl"))) {

			List<String> result = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".csv")).collect(Collectors.toList());

			
		      
		     
		      
			result.forEach(System.out::println);
			for (String var : result) 
			{ 
			    res=var.substring(94, var.length());
			    System.out.println("res est : " + res);
			    result2.add(res);
			}			 
		}

	 catch (IOException e) {
			e.printStackTrace();
		}
		/*for(int i =0;i<=result2.size();i++)
		{
			res = result2[i].substring(18, 29);
		}*/
		return result2;
	}
	
	
	
	@GetMapping("/findCriBl")
	public List<String> getCriBl() {
		List<String> result2=new ArrayList<String>();
		String res = "";
		try (Stream<Path> walk = Files.walk(Paths.get("B:\\stage Atos\\stage\\pidi-intranet\\pidi-intranet-client-master\\src\\assets\\fichiers\\expo cri bl"))) {

			List<String> result = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".csv")).collect(Collectors.toList());
			result.forEach(System.out::println);

			for (String var : result) 
			{ 
			    res=var.substring(94, var.length());
			    System.out.println("res est : " + res);
			    result2.add(res);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result2;
	}
	
	@GetMapping("/findCriCli")
	public List<String> getCriCli() {
		List<String> result2=new ArrayList<String>();
		String res = "";
		try (Stream<Path> walk = Files.walk(Paths.get("B:\\stage Atos\\stage\\pidi-intranet\\pidi-intranet-client-master\\src\\assets\\fichiers\\expo cri cli"))) {

			List<String> result = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".csv")).collect(Collectors.toList());
			result.forEach(System.out::println);

			for (String var : result) 
			{ 
			    res=var.substring(95, var.length());
			    System.out.println("res est : " + res);
			    result2.add(res);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result2;
	}
	
	@GetMapping("/findOtBl")
	public List<String> getOtBl() {
		List<String> result2=new ArrayList<String>();
		String res = "";
		try (Stream<Path> walk = Files.walk(Paths.get("B:\\stage Atos\\stage\\pidi-intranet\\pidi-intranet-client-master\\src\\assets\\fichiers\\expo ot bl"))) {

			List<String> result = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".csv")).collect(Collectors.toList());
			result.forEach(System.out::println);

			for (String var : result) 
			{ 
			    res=var.substring(93, var.length());
			    System.out.println("res est : " + res);
			    result2.add(res);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result2;
	}
	
	@GetMapping("/findOtCli")
	public List<String> getOtCli() {
		List<String> result2=new ArrayList<String>();
		String res = "";
		try (Stream<Path> walk = Files.walk(Paths.get("B:\\stage Atos\\stage\\pidi-intranet\\pidi-intranet-client-master\\src\\assets\\fichiers\\expo ot cli"))) {

			List<String> result = walk.map(x -> x.toString())
					.filter(f -> f.endsWith(".csv")).collect(Collectors.toList());
			result.forEach(System.out::println);

			for (String var : result) 
			{ 
			    res=var.substring(94, var.length());
			    System.out.println("res est : " + res);
			    result2.add(res);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result2;
	}
	
    

	
	
}
