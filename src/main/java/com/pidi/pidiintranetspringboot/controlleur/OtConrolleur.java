package com.pidi.pidiintranetspringboot.controlleur;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pidi.pidiintranetspringboot.models.OT;
import com.pidi.pidiintranetspringboot.repository.OtRepo;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/ot")
public class OtConrolleur {
	@PersistenceContext
    public EntityManager em;
	
	@Autowired
	private OtRepo otRepo;
	
	@GetMapping("/findall")
	public List<OT> getNotes() {
		return otRepo.findAll();
	}

	@PostMapping("/add")
	public OT createNote(@RequestBody OT cpu) {
		return otRepo.save(cpu);
	}
	
	/*@GetMapping("/getAnnule")
	public List<OT> getAnnule() {
		
    Query query = (Query) em.createQuery("select o.OT_DATE_ANNULATION from OT o");
    
    List<Object[]> rows =(List<Object[]>) ((javax.persistence.Query) query).getResultList();
    List<OT> result = new ArrayList<>(rows.size());
    for (Object[] row : rows) {
        result.add(new OT((int) row[0],(Date) row[1]));
    }
    System.out.println(result);
    
    return result;
	}*/
	
	@GetMapping("/getEtaAnnule")
	public List<Object[]> getEtaAnnule() {
		/*List<OT> result = new ArrayList<>(rows.size());
	    for (Object[] row : rows) {
	        result.add(new OT((int) row[0],(Date) row[1]));
	    }*/
		return otRepo.findEtaAnnule();
	}
	
	@GetMapping("/getEtaEnCours")
	public List<Object[]> getEtaEnCours() {
		return otRepo.findEtaEnCours();
	}
	
	
	@GetMapping("/getEtaCloture")
	public List<Object[]> getEtaCloture() {
		return otRepo.findEtaCloutre();
	}
	
	@GetMapping("/getEtaErreur")
	public List<Object[]> getEtaErreur() {
		return otRepo.findEtaErreur();
	}
	////////////////////////
	@GetMapping("/getNbrErreur")
	public int getNbrErreur() {
		return otRepo.findNbrErreur();
	}
	
	@GetMapping("/getNbrCloture")
	public int getNbrCloture() {
		return otRepo.findNbrCloture();
	}
	
	@GetMapping("/getNbrEnCours")
	public int getNbrEnCours() {
		return otRepo.findNbrEnCours();
	}

	@GetMapping("/getNbrAnnule")
	public int getNbrAnnule() {
		return otRepo.findNbrAnnule();
	}
	
}