package com.pidi.pidiintranetspringboot.controlleur;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pidi.pidiintranetspringboot.models.OT;
import com.pidi.pidiintranetspringboot.repository.OtRepo;
import com.pidi.pidiintranetspringboot.repository.TableauPilotageRepo;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/TableauPilotage")
public class TableauPilotageControleur {
		
		@Autowired
		private TableauPilotageRepo tabPiloRepo;
		
		/*@GetMapping("/findOt{nd}")
		public List<Object[]> getOt(@PathVariable Integer nd) {
			return tabPiloRepo.findOt(nd);
		}*/
		
		@GetMapping("/findOtClient")
		public List<Object[]> getOtClient() {
			return tabPiloRepo.findOtClient();
		}
		
		@GetMapping("/findOtBl")
		public List<Object[]> getOtBl() {
			return tabPiloRepo.findOtBl();
		}
		
		/*@GetMapping("/findOt/{id}")
		public Optional<OT> getOt(@PathVariable Long id) {
			return tabPiloRepo.findById(id);}*/
		
		@GetMapping("/findOt/{id}")
		public Object getOt(@PathVariable Long id) {
			return tabPiloRepo.findId(id);
			}
		
		
		/*@GetMapping("/findOt")
		public List<OT> getOt() {
			return tabPiloRepo.findAll();
		}*/
}
