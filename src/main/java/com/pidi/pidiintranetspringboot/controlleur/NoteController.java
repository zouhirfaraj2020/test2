package com.pidi.pidiintranetspringboot.controlleur;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pidi.pidiintranetspringboot.models.Note;
import com.pidi.pidiintranetspringboot.repository.NoteRepository;



@RestController
@RequestMapping("/note")
public class NoteController {



	@Autowired
	private NoteRepository noteRepository;
	
	@GetMapping("/findall")
	public List<Note> getNotes() {
		return noteRepository.findAll();
	}

	@GetMapping("/findone/{id}")
	public Optional<Note> getNote(@PathVariable Long id) {
		return noteRepository.findById(id);
	}

	@DeleteMapping("/delete/{id}")
	public boolean deleteNote(@PathVariable Long id) {
		noteRepository.deleteById(id);
		return true;
	}

	@PutMapping("/update")
	public Note updateNote(@RequestBody Note cpu) {
		return noteRepository.save(cpu);
	}

	@PostMapping("/add")
	public Note createNote(@RequestBody Note cpu) {
		return noteRepository.save(cpu);
	}

}

