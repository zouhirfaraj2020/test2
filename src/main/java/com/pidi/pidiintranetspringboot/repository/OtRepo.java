package com.pidi.pidiintranetspringboot.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.pidi.pidiintranetspringboot.models.OT;

@Repository
public interface OtRepo extends JpaRepository<OT, Long>{

	
	@Query( value = "SELECT count(*),OT_DATE_ANNULATION FROM ot where eta_id=2 and OT_DATE_ANNULATION > current_date - interval '7 days' group by OT_DATE_ANNULATION order by OT_DATE_ANNULATION", nativeQuery = true)
	List<Object[]> findEtaAnnule();
	
	
	@Query( value = "SELECT count(*),OT_DATE_ETAT_PIDI FROM ot where eta_id in (1,6) and OT_DATE_ETAT_PIDI > current_date - interval '7 days' group by OT_DATE_ETAT_PIDI order by OT_DATE_ETAT_PIDI", nativeQuery = true)
	List<Object[]> findEtaEnCours();
	
	@Query( value = "select count(O.OT_ID), CR.CRI_DATE_CLOTURE from OT O,CRI CR where O.OT_ID=CR.OT_ID and O.eta_id=3 and CR.CRI_DATE_CLOTURE "
			+ "> current_date - interval '7 days' group by CR.CRI_DATE_CLOTURE order by CR.CRI_DATE_CLOTURE", nativeQuery = true)
	List<Object[]> findEtaCloutre();
	
	@Query( value = "select count(O.OT_ID), CRT.ERR_DATE from OT O,CRI CR,CRI_RETOUR CRT where O.OT_ID=CR.OT_ID and CR.CRI_ID=CRT.CRI_ID and O.eta_id=4 and CRT.ERR_DATE > current_date - interval '7 days' group by CRT.ERR_DATE order by CRT.ERR_DATE", nativeQuery = true)
	List<Object[]> findEtaErreur();
	
	////////////////////////////////////
	@Query( value = "select count(O.OT_ID) from OT O,CRI CR,CRI_RETOUR CRT  where O.OT_ID=CR.OT_ID and CR.CRI_ID=CRT.CRI_ID and O.eta_id=4", nativeQuery = true)
	int findNbrErreur();
	
	@Query( value = "select count(O.OT_ID) from OT O,CRI CR where O.OT_ID=CR.OT_ID and O.eta_id=3",
			nativeQuery = true)
	int findNbrCloture();
	
	@Query( value = "SELECT count(*) FROM ot where eta_id in (1,6)", nativeQuery = true)
	int findNbrEnCours();
	
	@Query( value = "SELECT count(*) FROM ot where eta_id=2", nativeQuery = true)
	int findNbrAnnule();
	
}
