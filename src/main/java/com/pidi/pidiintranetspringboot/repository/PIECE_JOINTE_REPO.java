package com.pidi.pidiintranetspringboot.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pidi.pidiintranetspringboot.models.OT;
import com.pidi.pidiintranetspringboot.models.PIECE_JOINTE;

public interface PIECE_JOINTE_REPO extends JpaRepository<PIECE_JOINTE, Long> {

	
	//@Query( value = "SELECT * FROM PIECE_JOINTE", nativeQuery = true)
	List<PIECE_JOINTE> findAll();
	
	
	@Query( value = "SELECT * from PIECE_JOINTE p where p.OT_ID=?1", nativeQuery = true)
	//SELECT * from PIECE_JOINTE p, CRI c where c.ot_id=p.OT_ID and p.OT_ID=5 
	List<PIECE_JOINTE> findByOt(Long id_ot);
	
	@Query( value = "SELECT * from PIECE_JOINTE p , CRI c where c.cri_id=p.cri_ID and p.OT_ID=?1", nativeQuery = true)
	//SELECT * from PIECE_JOINTE p, CRI c where c.ot_id=p.OT_ID and p.OT_ID=5 
	List<PIECE_JOINTE> findByOtCri(Long id_ot);
	
}
