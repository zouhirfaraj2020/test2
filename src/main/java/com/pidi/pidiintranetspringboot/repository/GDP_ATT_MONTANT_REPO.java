package com.pidi.pidiintranetspringboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pidi.pidiintranetspringboot.models.GDP_ATT_MONTANT;
import com.pidi.pidiintranetspringboot.models.OT;

@Repository
public interface GDP_ATT_MONTANT_REPO  extends JpaRepository<GDP_ATT_MONTANT, Long> {

	@Query( value = "SELECT count(*),GDP_ATT_DATE_ACQUIT_REJET_GDP FROM GDP_ATT_MONTANT where GDP_ATT_ETAT_ID =7 and GDP_ATT_DATE_ACQUIT_REJET_GDP > current_date - interval '7 days' group by GDP_ATT_DATE_ACQUIT_REJET_GDP order by GDP_ATT_DATE_ACQUIT_REJET_GDP", nativeQuery = true)
	List<Object[]> findRejetGdp();
	
	@Query( value = "SELECT count(*),GDP_ATT_DATE_ACQUIT_REJET_RQF FROM GDP_ATT_MONTANT where GDP_ATT_ETAT_ID =4 and GDP_ATT_DATE_ACQUIT_REJET_RQF > current_date - interval '7 days' group by GDP_ATT_DATE_ACQUIT_REJET_RQF order by GDP_ATT_DATE_ACQUIT_REJET_RQF", nativeQuery = true)
	List<Object[]> findRejetRqf();
	
	@Query( value = "SELECT count(*),GDP_ATT_DATE_ACQUIT_REJET_GDP FROM GDP_ATT_MONTANT where GDP_ATT_ETAT_ID =8 and GDP_ATT_DATE_ACQUIT_REJET_GDP > current_date - interval '7 days' group by GDP_ATT_DATE_ACQUIT_REJET_GDP order by GDP_ATT_DATE_ACQUIT_REJET_GDP", nativeQuery = true)
	List<Object[]> findValidationGdp();
	
	@Query( value = "SELECT count(*),GDP_ATT_DATE_CREATION  FROM GDP_ATT_MONTANT where GDP_ATT_ETAT_ID=3 and GDP_ATT_DATE_CREATION > current_date - interval '7 days' group by GDP_ATT_DATE_CREATION order by GDP_ATT_DATE_CREATION", nativeQuery = true)
	List<Object[]> findAttenteRqf();
	
	@Query( value = "SELECT count(*),GDP_ATT_DATE_ENVOI_GDP FROM GDP_ATT_MONTANT where GDP_ATT_ETAT_ID=6 and GDP_ATT_DATE_ENVOI_GDP > current_date - interval '7 days' group by GDP_ATT_DATE_ENVOI_GDP order by GDP_ATT_DATE_ENVOI_GDP", nativeQuery = true)
	List<Object[]> findAttenteGdp();
	
	@Query( value = "SELECT count(*),GDP_ATT_DATE_CREATION  FROM GDP_ATT_MONTANT where GDP_ATT_ETAT_ID=10 and GDP_ATT_DATE_CREATION > current_date - interval '7 days' group by GDP_ATT_DATE_CREATION  order by GDP_ATT_DATE_CREATION ", nativeQuery = true)
	List<Object[]> findAttenteDt();
}

