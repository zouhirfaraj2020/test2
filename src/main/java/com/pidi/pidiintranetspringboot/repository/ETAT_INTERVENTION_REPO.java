package com.pidi.pidiintranetspringboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pidi.pidiintranetspringboot.models.ETAT_INTERVENTION;
import com.pidi.pidiintranetspringboot.models.OT;

@Repository
public interface ETAT_INTERVENTION_REPO  extends JpaRepository<ETAT_INTERVENTION, Long>{

		
}
