package com.pidi.pidiintranetspringboot.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pidi.pidiintranetspringboot.models.DBFile;

@Repository
public interface DBFileRepository extends JpaRepository<DBFile, String> {

}