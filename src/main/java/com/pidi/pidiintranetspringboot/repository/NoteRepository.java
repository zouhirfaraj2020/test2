package com.pidi.pidiintranetspringboot.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pidi.pidiintranetspringboot.models.Note;


@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

}

