package com.pidi.pidiintranetspringboot.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.pidi.pidiintranetspringboot.models.Test1;

@Repository
public interface RepoTest1 extends JpaRepository<Test1, Long>{
}
