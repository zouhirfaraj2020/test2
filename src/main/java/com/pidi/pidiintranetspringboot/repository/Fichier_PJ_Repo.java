package com.pidi.pidiintranetspringboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pidi.pidiintranetspringboot.models.Fichier_PJ;

public interface Fichier_PJ_Repo extends JpaRepository<Fichier_PJ, Long> {

}
