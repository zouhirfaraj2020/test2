package com.pidi.pidiintranetspringboot.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pidi.pidiintranetspringboot.models.OT;


@Repository
public interface TableauPilotageRepo extends JpaRepository<OT, Long>{

	@Query( value = "select o.OT_ETAT_PIDI, en.ENT_LIBELLE, o.UI_CODE, o.OT_NUM_TICKET_OCEANE, n.ND, o.OT_DATE_RDV, o.OT_DATE_PLANIF, o.OT_ENGAGEMENT\r\n" + 
			", o.ACT_CODE , o.PRO_CODE from OT o, UI u, ND n, ENTITE en where o.UI_CODE=u.UI_CODE and o.OT_ID=n.OT_ID and u.ENT_ID=en.ENT_ID and n.ND=?1", nativeQuery = true)
	List<Object[]> findOt(Integer nd);
	
	@Query(value = "\r\n" + 
			"select o.OT_ETAT_PIDI, en.ENT_LIBELLE, o.UI_CODE, o.OT_NUM_TICKET_OCEANE,\r\n" + 
			"n.ND, o.OT_CLIENT_UTILISATEUR, o.OT_DATE_RDV, o.OT_DATE_PLANIF,o.OT_CONTRATS,\r\n" + 
			"o.OT_ENGAGEMENT, o.ACT_CODE , o.PRO_CODE,o.OT_CENTRE, o.OT_LIEU, et.ETL_LIBELLE\r\n" + 
			",o.ot_etl_id, o.ot_ind_rdv, o.ot_ind_risque, dom.DOM_LIBELLE, ea.eta_libelle, o.OT_ID\r\n" + 
			"from OT o, UI u, ND n, ENTITE en, ETL et, ACTIVITE ac, DOMAINE dom, ETAT_INTERVENTION ea "
			+ "where o.UI_CODE=u.UI_CODE and o.OT_ID=n.OT_ID \r\n" + 
			"and u.ENT_ID=en.ENT_ID and et.ETL_ID=o.OT_ETL_ID and "
			+ "o.ACT_CODE=ac.ACT_CODE and ac.DOM_CODE=dom.DOM_CODE "
			+ "and ea.eta_id=o.eta_id and u.ui_bl=0", nativeQuery = true)
	List<Object[]> findOtClient();
	
	@Query(value = "\r\n" + 
			"select o.OT_ETAT_PIDI, en.ENT_LIBELLE, o.UI_CODE, o.OT_NUM_TICKET_OCEANE,\r\n" + 
			"n.ND, o.OT_CLIENT_UTILISATEUR, o.OT_DATE_RDV, o.OT_DATE_PLANIF,o.OT_CONTRATS,\r\n" + 
			"o.OT_ENGAGEMENT, o.ACT_CODE , o.PRO_CODE,o.OT_CENTRE, o.OT_LIEU, et.ETL_LIBELLE\r\n" + 
			",o.ot_etl_id, o.ot_ind_rdv, o.ot_ind_risque, dom.DOM_LIBELLE, ea.eta_libelle, o.OT_ID\r\n" + 
			"from OT o, UI u, ND n, ENTITE en, ETL et, ACTIVITE ac, DOMAINE dom, ETAT_INTERVENTION ea where"
			+ " o.UI_CODE=u.UI_CODE and o.OT_ID=n.OT_ID \r\n" + 
			"and u.ENT_ID=en.ENT_ID and et.ETL_ID=o.OT_ETL_ID and o.ACT_CODE=ac.ACT_CODE and ac.DOM_CODE=dom.DOM_CODE and ea.eta_id=o.eta_id and u.ui_bl=1", nativeQuery = true)
	List<Object[]> findOtBl();
	
	@Query( value = "SELECT \r\n" + 
			"n.ND, o.act_code, o.pro_code, o.ui_code, o.ot_date_planif, \r\n" + 
			"o.ot_client_utilisateur, o.ot_date_contractuelle, dom.DOM_LIBELLE\r\n" + 
			"FROM ot o, DOMAINE dom,ND n, ACTIVITE ac where o.OT_ID=n.OT_ID and o.ACT_CODE=ac.ACT_CODE \r\n" + 
			"and ac.DOM_CODE=dom.DOM_CODE and o.OT_ID=?1", nativeQuery = true)
	Object findId(Long nd);
	
	
}
