package com.pidi.pidiintranetspringboot.models;

import java.io.File;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

import com.opencsv.bean.CsvBindByName;

@Entity
@Table(name="test1")
public class Test1 {

	/*@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@CsvBindByName
	private int id;
	 @CsvBindByName
	private String nom; 
	 @CsvBindByName
	private String prenom;
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	@Override
	public String toString() {
		return "Test1 [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	public Test1(int id, String nom, String prenom) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	} 
	
	public Test1() {
		super();
	} */
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	    public String FirstName ;
	    public String LastName ;
	    public String Email ;
	   // public MultipartFile[] ImgFile;
	    //public DateTime EnteredDate;
		public Test1() {
			super();
			// TODO Auto-generated constructor stub
		}
		public Test1(int id, String firstName, String lastName, String email/*, MultipartFile[] imgFile*/) {
			super();
			this.id = id;
			FirstName = firstName;
			LastName = lastName;
			Email = email;
			//ImgFile = imgFile;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getFirstName() {
			return FirstName;
		}
		public void setFirstName(String firstName) {
			FirstName = firstName;
		}
		public String getLastName() {
			return LastName;
		}
		public void setLastName(String lastName) {
			LastName = lastName;
		}
		public String getEmail() {
			return Email;
		}
		public void setEmail(String email) {
			Email = email;
		}
		/*public MultipartFile[] getImgFile() {
			return ImgFile;
		}
		public void setImgFile(MultipartFile[] imgFile) {
			ImgFile = imgFile;
		}*/
	    
	
}
