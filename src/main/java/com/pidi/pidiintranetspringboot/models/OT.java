package com.pidi.pidiintranetspringboot.models;

import java.security.Timestamp;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Null;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
//import com.sun.istack.NotNull;

@Entity
@Table(name="OT")
public class OT {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int OT_ID; 
	private int ETA_ID; 
	private Date OT_DATE_ANNULATION;
	private Date OT_DATE_ETAT_PIDI;
	private int OT_ETL_ID;
	//@NotNull
	private String ACT_CODE; 
	//@NotNull
	private String PRO_CODE; 
	private String UI_CODE; 
	private String OT_ETAT_PIDI; 
	private String OT_UI_CODE; 
	private String OT_NUM_TICKET_OCEANE;
	private Date OT_DATE_RDV;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date OT_DATE_PLANIF; 
	private String OT_ENGAGEMENT;
	private String OT_CLIENT_UTILISATEUR;
	private String OT_LIEU; 
	private String OT_CONTRATS; 
	private String OT_CENTRE; 
	@Null
	@Column(name = "OT_IND_RDV")
	private int OT_IND_RDV; 
	@Null
	@Column(name = "OT_IND_RISQUE")
	private int OT_IND_RISQUE; 
	
	
	
	
	//@NotNull
	private String GPC_CODE; 
	private String OT_ND_PRINCIPAL; 
	private String OT_NUMINT; 

	//@NotNull
	private String OT_REF_INT; 
	
	private Date OT_DATE_CREATION; 
	private Date OT_DATE_MODIFICATION; 
	private Date OT_DATE_RECEPTION_SI; 

	private Date  OT_DATE_CONTRACTUELLE; 
	private Date OT_DATE_ENVOI_SI; 
	private String OT_CLIENT_TITULAIRE; 
	private String OT_MODE_ENVOI_SI;
	
	@Null
	@Column(name = "OT_VALIDE")
	private int OT_VALIDE; 
	@Null
	@Column(name = "OT_PJ_ATTENDU")
	private int OT_PJ_ATTENDU; 
	private String OT_ETAT_GPC; 
	private String OT_MARGE;
	private String OT_SEGMENT_MARCHE;  
	//@NotNull
	private String UI_ID; 
	private String OT_ETL_CODE; 
	@Null
	@Column(name = "OT_ENVOI_ETECH_ID")
	private int OT_ENVOI_ETECH_ID;
	private String OT_COMMENTAIRE_ANNUL; 
	private String OT_CODE_AGENT; 
	private String OT_IND_OEB; 
	private String OT_ZONE; 
	@Null
	@Column(name = "OT_REF_OT_GROUPE")
	private int OT_REF_OT_GROUPE; 

	private String OT_CEDSI; 
	private Date OT_DLR; 
	private String OT_COMMUNE; 
	private String OT_ORIGINE; 
	@Null
	@Column(name = "OT_NB_ESSAI")
	private int OT_NB_ESSAI; 
	private String OT_NUM_CONTACT; 
	/*@Null
	@Column(name = "OT_APPEL_CLIENT")
	private int OT_APPEL_CLIENT; */
	private Date OT_DATE_TC; 
	private String OT_RESSOURCES; 
	private Date OT_DATE_ETAT_TC_PIDI; 
	private String OT_REF_COMMUNE; 
	private Date OT_DATE_PSG_ATTENTE_CLOTURE; 
	@Null
	@Column(name = "OT_NB_PJ_GEDAFFAIRE")
	private int OT_NB_PJ_GEDAFFAIRE; 
	private Date OT_DATE_RECEPT_PJ_GED; 
	private Date OT_DATE_CONSULT_PJ_GED;
	private String OT_GDP_CODE;
	@Null
	@Column(name = "OT_DFI_ID")
	private int OT_DFI_ID;
	private String OT_DEGROUPAGE;
	@Null
	@Column(name = "OT_HAS_DEVIS_FI")
	private int OT_HAS_DEVIS_FI;
	@Null
	@Column(name = "OT_PJGED_DEMANDE")
	private int OT_PJGED_DEMANDE;
	@Null
	@Column(name = "OT_ETAT_GDP_ID")
	private int OT_ETAT_GDP_ID;
	private Date OT_DATE_METIER_TA_PIDI;
	private Date OT_DATE_SYSTEM_TR_PIDI;
	private Date OT_DATE_SYSTEM_TC2_PIDI;
	private Date OT_DATE_METIER_TC2_PIDI;
	private Date OT_DATE_REPLANIFICATION_TR;
	@Null
	@Column(name = "OT_INDIC_RDV")
	private int OT_INDIC_RDV;
	private Date OT_DATE_RECEPT_MODIF_SI;
	private String OT_NUM_VOIE;
	private String OT_LIBELLE_VOIE;
	@Null
	@Column(name = "OT_ENVOYER_ATON")
	private int OT_ENVOYER_ATON;
	private Date OT_DATE_REPLANIFICATION;
	@Null
	@Column(name = "OT_ENVOYER_ETL")
	private int OT_ENVOYER_ETL;
	private String OT_COMMENTAIRE_SUIVI;
	private String OT_TYPE_DEBIT;
	private String OT_POINT_COUPURE;
	private String OT_TYPE_REGROUPEMENT;
	private String OT_OPERATEUR;
	private String OT_NUMPRESTACLIP;
	private Date OT_DATE_ANNONCEE;
	/*@Null
	@Column(name = "OT_CHANTIER_ID")
	private int OT_CHANTIER_ID;*/
	private Date OT_DATE_ECHANTILLON;
	private String OT_DTIO;
	private String OT_CATEGORIE_PLP;
	private String OT_NUM_PTO;
	private String OT_NUM_PORTE_CLIENT;
	@Null
	@Column(name = "OT_SDSL")
	private int OT_SDSL;
	private String OT_INDIC_COMPLEX;
	private String OT_BLOCNOTE;
	private String OT_TAG_PROCESS_CONTEXT;
	private String OT_REAFFECTE;
	@Null
	@Column(name = "OT_NB_TC")
	private int OT_NB_TC;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date lastUpdate;
	
	
	@OneToMany
    private Set<ETL> etl = new HashSet<ETL>(0);
	
	@OneToMany
    private Set<UI> ui = new HashSet<UI>(0);
	
	
		public OT() {
		super();
	}


		public OT(int oT_ID, int eTA_ID, Date oT_DATE_ANNULATION, Date oT_DATE_ETAT_PIDI, int oT_ETL_ID,
				String aCT_CODE, String pRO_CODE, String uI_CODE, String oT_ETAT_PIDI, String oT_UI_CODE,
				String oT_NUM_TICKET_OCEANE, Date oT_DATE_RDV, Date oT_DATE_PLANIF, String oT_ENGAGEMENT,
				String oT_CLIENT_UTILISATEUR, String oT_LIEU, String oT_CONTRATS, String oT_CENTRE,
				@Null int oT_IND_RDV, @Null int oT_IND_RISQUE, String gPC_CODE, String oT_ND_PRINCIPAL,
				String oT_NUMINT, String oT_REF_INT, Date oT_DATE_CREATION, Date oT_DATE_MODIFICATION,
				Date oT_DATE_RECEPTION_SI, Date  oT_DATE_CONTRACTUELLE, Date oT_DATE_ENVOI_SI,
				String oT_CLIENT_TITULAIRE, String oT_MODE_ENVOI_SI, @Null int oT_VALIDE, @Null int oT_PJ_ATTENDU,
				String oT_ETAT_GPC, String oT_MARGE, String oT_SEGMENT_MARCHE, String uI_ID, String oT_ETL_CODE,
				@Null int oT_ENVOI_ETECH_ID, String oT_COMMENTAIRE_ANNUL, String oT_CODE_AGENT, String oT_IND_OEB,
				String oT_ZONE, @Null int oT_REF_OT_GROUPE, String oT_CEDSI, Date oT_DLR, String oT_COMMUNE,
				String oT_ORIGINE, @Null int oT_NB_ESSAI, String oT_NUM_CONTACT, /*@Null int oT_APPEL_CLIENT,*/
				Date oT_DATE_TC, String oT_RESSOURCES, Date oT_DATE_ETAT_TC_PIDI, String oT_REF_COMMUNE,
				Date oT_DATE_PSG_ATTENTE_CLOTURE, @Null int oT_NB_PJ_GEDAFFAIRE, Date oT_DATE_RECEPT_PJ_GED,
				Date oT_DATE_CONSULT_PJ_GED, String oT_GDP_CODE, @Null int oT_DFI_ID, String oT_DEGROUPAGE,
				@Null int oT_HAS_DEVIS_FI, @Null int oT_PJGED_DEMANDE, @Null int oT_ETAT_GDP_ID,
				Date oT_DATE_METIER_TA_PIDI, Date oT_DATE_SYSTEM_TR_PIDI, Date oT_DATE_SYSTEM_TC2_PIDI,
				Date oT_DATE_METIER_TC2_PIDI, Date oT_DATE_REPLANIFICATION_TR, @Null int oT_INDIC_RDV,
				Date oT_DATE_RECEPT_MODIF_SI, String oT_NUM_VOIE, String oT_LIBELLE_VOIE, @Null int oT_ENVOYER_ATON,
				Date oT_DATE_REPLANIFICATION, @Null int oT_ENVOYER_ETL, String oT_COMMENTAIRE_SUIVI,
				String oT_TYPE_DEBIT, String oT_POINT_COUPURE, String oT_TYPE_REGROUPEMENT, String oT_OPERATEUR,
				String oT_NUMPRESTACLIP, Date oT_DATE_ANNONCEE, /*@Null int oT_CHANTIER_ID,*/ Date oT_DATE_ECHANTILLON,
				String oT_DTIO, String oT_CATEGORIE_PLP, String oT_NUM_PTO, String oT_NUM_PORTE_CLIENT,
				@Null int oT_SDSL, String oT_INDIC_COMPLEX, String oT_BLOCNOTE, String oT_TAG_PROCESS_CONTEXT,
				String oT_REAFFECTE, @Null int oT_NB_TC, Set<ETL> etl, Set<UI> ui) {
			super();
			OT_ID = oT_ID;
			ETA_ID = eTA_ID;
			OT_DATE_ANNULATION = oT_DATE_ANNULATION;
			OT_DATE_ETAT_PIDI = oT_DATE_ETAT_PIDI;
			OT_ETL_ID = oT_ETL_ID;
			ACT_CODE = aCT_CODE;
			PRO_CODE = pRO_CODE;
			UI_CODE = uI_CODE;
			OT_ETAT_PIDI = oT_ETAT_PIDI;
			OT_UI_CODE = oT_UI_CODE;
			OT_NUM_TICKET_OCEANE = oT_NUM_TICKET_OCEANE;
			OT_DATE_RDV = oT_DATE_RDV;
			OT_DATE_PLANIF = oT_DATE_PLANIF;
			OT_ENGAGEMENT = oT_ENGAGEMENT;
			OT_CLIENT_UTILISATEUR = oT_CLIENT_UTILISATEUR;
			OT_LIEU = oT_LIEU;
			OT_CONTRATS = oT_CONTRATS;
			OT_CENTRE = oT_CENTRE;
			OT_IND_RDV = oT_IND_RDV;
			OT_IND_RISQUE = oT_IND_RISQUE;
			GPC_CODE = gPC_CODE;
			OT_ND_PRINCIPAL = oT_ND_PRINCIPAL;
			OT_NUMINT = oT_NUMINT;
			OT_REF_INT = oT_REF_INT;
			OT_DATE_CREATION = oT_DATE_CREATION;
			OT_DATE_MODIFICATION = oT_DATE_MODIFICATION;
			OT_DATE_RECEPTION_SI = oT_DATE_RECEPTION_SI;
			OT_DATE_CONTRACTUELLE = oT_DATE_CONTRACTUELLE;
			OT_DATE_ENVOI_SI = oT_DATE_ENVOI_SI;
			OT_CLIENT_TITULAIRE = oT_CLIENT_TITULAIRE;
			OT_MODE_ENVOI_SI = oT_MODE_ENVOI_SI;
			OT_VALIDE = oT_VALIDE;
			OT_PJ_ATTENDU = oT_PJ_ATTENDU;
			OT_ETAT_GPC = oT_ETAT_GPC;
			OT_MARGE = oT_MARGE;
			OT_SEGMENT_MARCHE = oT_SEGMENT_MARCHE;
			UI_ID = uI_ID;
			OT_ETL_CODE = oT_ETL_CODE;
			OT_ENVOI_ETECH_ID = oT_ENVOI_ETECH_ID;
			OT_COMMENTAIRE_ANNUL = oT_COMMENTAIRE_ANNUL;
			OT_CODE_AGENT = oT_CODE_AGENT;
			OT_IND_OEB = oT_IND_OEB;
			OT_ZONE = oT_ZONE;
			OT_REF_OT_GROUPE = oT_REF_OT_GROUPE;
			OT_CEDSI = oT_CEDSI;
			OT_DLR = oT_DLR;
			OT_COMMUNE = oT_COMMUNE;
			OT_ORIGINE = oT_ORIGINE;
			OT_NB_ESSAI = oT_NB_ESSAI;
			OT_NUM_CONTACT = oT_NUM_CONTACT;
			/*OT_APPEL_CLIENT = oT_APPEL_CLIENT;*/
			OT_DATE_TC = oT_DATE_TC;
			OT_RESSOURCES = oT_RESSOURCES;
			OT_DATE_ETAT_TC_PIDI = oT_DATE_ETAT_TC_PIDI;
			OT_REF_COMMUNE = oT_REF_COMMUNE;
			OT_DATE_PSG_ATTENTE_CLOTURE = oT_DATE_PSG_ATTENTE_CLOTURE;
			OT_NB_PJ_GEDAFFAIRE = oT_NB_PJ_GEDAFFAIRE;
			OT_DATE_RECEPT_PJ_GED = oT_DATE_RECEPT_PJ_GED;
			OT_DATE_CONSULT_PJ_GED = oT_DATE_CONSULT_PJ_GED;
			OT_GDP_CODE = oT_GDP_CODE;
			OT_DFI_ID = oT_DFI_ID;
			OT_DEGROUPAGE = oT_DEGROUPAGE;
			OT_HAS_DEVIS_FI = oT_HAS_DEVIS_FI;
			OT_PJGED_DEMANDE = oT_PJGED_DEMANDE;
			OT_ETAT_GDP_ID = oT_ETAT_GDP_ID;
			OT_DATE_METIER_TA_PIDI = oT_DATE_METIER_TA_PIDI;
			OT_DATE_SYSTEM_TR_PIDI = oT_DATE_SYSTEM_TR_PIDI;
			OT_DATE_SYSTEM_TC2_PIDI = oT_DATE_SYSTEM_TC2_PIDI;
			OT_DATE_METIER_TC2_PIDI = oT_DATE_METIER_TC2_PIDI;
			OT_DATE_REPLANIFICATION_TR = oT_DATE_REPLANIFICATION_TR;
			OT_INDIC_RDV = oT_INDIC_RDV;
			OT_DATE_RECEPT_MODIF_SI = oT_DATE_RECEPT_MODIF_SI;
			OT_NUM_VOIE = oT_NUM_VOIE;
			OT_LIBELLE_VOIE = oT_LIBELLE_VOIE;
			OT_ENVOYER_ATON = oT_ENVOYER_ATON;
			OT_DATE_REPLANIFICATION = oT_DATE_REPLANIFICATION;
			OT_ENVOYER_ETL = oT_ENVOYER_ETL;
			OT_COMMENTAIRE_SUIVI = oT_COMMENTAIRE_SUIVI;
			OT_TYPE_DEBIT = oT_TYPE_DEBIT;
			OT_POINT_COUPURE = oT_POINT_COUPURE;
			OT_TYPE_REGROUPEMENT = oT_TYPE_REGROUPEMENT;
			OT_OPERATEUR = oT_OPERATEUR;
			OT_NUMPRESTACLIP = oT_NUMPRESTACLIP;
			OT_DATE_ANNONCEE = oT_DATE_ANNONCEE;
			/*OT_CHANTIER_ID = oT_CHANTIER_ID;*/
			OT_DATE_ECHANTILLON = oT_DATE_ECHANTILLON;
			OT_DTIO = oT_DTIO;
			OT_CATEGORIE_PLP = oT_CATEGORIE_PLP;
			OT_NUM_PTO = oT_NUM_PTO;
			OT_NUM_PORTE_CLIENT = oT_NUM_PORTE_CLIENT;
			OT_SDSL = oT_SDSL;
			OT_INDIC_COMPLEX = oT_INDIC_COMPLEX;
			OT_BLOCNOTE = oT_BLOCNOTE;
			OT_TAG_PROCESS_CONTEXT = oT_TAG_PROCESS_CONTEXT;
			OT_REAFFECTE = oT_REAFFECTE;
			OT_NB_TC = oT_NB_TC;
			this.etl = etl;
			this.ui = ui;
		}


		public int getOT_ID() {
			return OT_ID;
		}


		public void setOT_ID(int oT_ID) {
			OT_ID = oT_ID;
		}


		public int getETA_ID() {
			return ETA_ID;
		}


		public void setETA_ID(int eTA_ID) {
			ETA_ID = eTA_ID;
		}


		public Date getOT_DATE_ANNULATION() {
			return OT_DATE_ANNULATION;
		}


		public void setOT_DATE_ANNULATION(Date oT_DATE_ANNULATION) {
			OT_DATE_ANNULATION = oT_DATE_ANNULATION;
		}


		public Date getOT_DATE_ETAT_PIDI() {
			return OT_DATE_ETAT_PIDI;
		}


		public void setOT_DATE_ETAT_PIDI(Date oT_DATE_ETAT_PIDI) {
			OT_DATE_ETAT_PIDI = oT_DATE_ETAT_PIDI;
		}


		public int getOT_ETL_ID() {
			return OT_ETL_ID;
		}


		public void setOT_ETL_ID(int oT_ETL_ID) {
			OT_ETL_ID = oT_ETL_ID;
		}


		public String getACT_CODE() {
			return ACT_CODE;
		}


		public void setACT_CODE(String aCT_CODE) {
			ACT_CODE = aCT_CODE;
		}


		public String getPRO_CODE() {
			return PRO_CODE;
		}


		public void setPRO_CODE(String pRO_CODE) {
			PRO_CODE = pRO_CODE;
		}


		public String getUI_CODE() {
			return UI_CODE;
		}


		public void setUI_CODE(String uI_CODE) {
			UI_CODE = uI_CODE;
		}


		public String getOT_ETAT_PIDI() {
			return OT_ETAT_PIDI;
		}


		public void setOT_ETAT_PIDI(String oT_ETAT_PIDI) {
			OT_ETAT_PIDI = oT_ETAT_PIDI;
		}


		public String getOT_UI_CODE() {
			return OT_UI_CODE;
		}


		public void setOT_UI_CODE(String oT_UI_CODE) {
			OT_UI_CODE = oT_UI_CODE;
		}


		public String getOT_NUM_TICKET_OCEANE() {
			return OT_NUM_TICKET_OCEANE;
		}


		public void setOT_NUM_TICKET_OCEANE(String oT_NUM_TICKET_OCEANE) {
			OT_NUM_TICKET_OCEANE = oT_NUM_TICKET_OCEANE;
		}


		public Date getOT_DATE_RDV() {
			return OT_DATE_RDV;
		}


		public void setOT_DATE_RDV(Date oT_DATE_RDV) {
			OT_DATE_RDV = oT_DATE_RDV;
		}


		public Date getOT_DATE_PLANIF() {
			return OT_DATE_PLANIF;
		}


		public void setOT_DATE_PLANIF(Date oT_DATE_PLANIF) {
			OT_DATE_PLANIF = oT_DATE_PLANIF;
		}


		public String getOT_ENGAGEMENT() {
			return OT_ENGAGEMENT;
		}


		public void setOT_ENGAGEMENT(String oT_ENGAGEMENT) {
			OT_ENGAGEMENT = oT_ENGAGEMENT;
		}


		public String getOT_CLIENT_UTILISATEUR() {
			return OT_CLIENT_UTILISATEUR;
		}


		public void setOT_CLIENT_UTILISATEUR(String oT_CLIENT_UTILISATEUR) {
			OT_CLIENT_UTILISATEUR = oT_CLIENT_UTILISATEUR;
		}


		public String getOT_LIEU() {
			return OT_LIEU;
		}


		public void setOT_LIEU(String oT_LIEU) {
			OT_LIEU = oT_LIEU;
		}


		public String getOT_CONTRATS() {
			return OT_CONTRATS;
		}


		public void setOT_CONTRATS(String oT_CONTRATS) {
			OT_CONTRATS = oT_CONTRATS;
		}


		public String getOT_CENTRE() {
			return OT_CENTRE;
		}


		public void setOT_CENTRE(String oT_CENTRE) {
			OT_CENTRE = oT_CENTRE;
		}


		public int getOT_IND_RDV() {
			return OT_IND_RDV;
		}


		public void setOT_IND_RDV(int oT_IND_RDV) {
			OT_IND_RDV = oT_IND_RDV;
		}


		public int getOT_IND_RISQUE() {
			return OT_IND_RISQUE;
		}


		public void setOT_IND_RISQUE(int oT_IND_RISQUE) {
			OT_IND_RISQUE = oT_IND_RISQUE;
		}


		public String getGPC_CODE() {
			return GPC_CODE;
		}


		public void setGPC_CODE(String gPC_CODE) {
			GPC_CODE = gPC_CODE;
		}


		public String getOT_ND_PRINCIPAL() {
			return OT_ND_PRINCIPAL;
		}


		public void setOT_ND_PRINCIPAL(String oT_ND_PRINCIPAL) {
			OT_ND_PRINCIPAL = oT_ND_PRINCIPAL;
		}


		public String getOT_NUMINT() {
			return OT_NUMINT;
		}


		public void setOT_NUMINT(String oT_NUMINT) {
			OT_NUMINT = oT_NUMINT;
		}


		public String getOT_REF_INT() {
			return OT_REF_INT;
		}


		public void setOT_REF_INT(String oT_REF_INT) {
			OT_REF_INT = oT_REF_INT;
		}


		public Date getOT_DATE_CREATION() {
			return OT_DATE_CREATION;
		}


		public void setOT_DATE_CREATION(Date oT_DATE_CREATION) {
			OT_DATE_CREATION = oT_DATE_CREATION;
		}


		public Date getOT_DATE_MODIFICATION() {
			return OT_DATE_MODIFICATION;
		}


		public void setOT_DATE_MODIFICATION(Date oT_DATE_MODIFICATION) {
			OT_DATE_MODIFICATION = oT_DATE_MODIFICATION;
		}


		public Date getOT_DATE_RECEPTION_SI() {
			return OT_DATE_RECEPTION_SI;
		}


		public void setOT_DATE_RECEPTION_SI(Date oT_DATE_RECEPTION_SI) {
			OT_DATE_RECEPTION_SI = oT_DATE_RECEPTION_SI;
		}


		public Date  getOT_DATE_CONTRACTUELLE() {
			return OT_DATE_CONTRACTUELLE;
		}


		public void setOT_DATE_CONTRACTUELLE(Date  oT_DATE_CONTRACTUELLE) {
			OT_DATE_CONTRACTUELLE = oT_DATE_CONTRACTUELLE;
		}


		public Date getOT_DATE_ENVOI_SI() {
			return OT_DATE_ENVOI_SI;
		}


		public void setOT_DATE_ENVOI_SI(Date oT_DATE_ENVOI_SI) {
			OT_DATE_ENVOI_SI = oT_DATE_ENVOI_SI;
		}


		public String getOT_CLIENT_TITULAIRE() {
			return OT_CLIENT_TITULAIRE;
		}


		public void setOT_CLIENT_TITULAIRE(String oT_CLIENT_TITULAIRE) {
			OT_CLIENT_TITULAIRE = oT_CLIENT_TITULAIRE;
		}


		public String getOT_MODE_ENVOI_SI() {
			return OT_MODE_ENVOI_SI;
		}


		public void setOT_MODE_ENVOI_SI(String oT_MODE_ENVOI_SI) {
			OT_MODE_ENVOI_SI = oT_MODE_ENVOI_SI;
		}


		public int getOT_VALIDE() {
			return OT_VALIDE;
		}


		public void setOT_VALIDE(int oT_VALIDE) {
			OT_VALIDE = oT_VALIDE;
		}


		public int getOT_PJ_ATTENDU() {
			return OT_PJ_ATTENDU;
		}


		public void setOT_PJ_ATTENDU(int oT_PJ_ATTENDU) {
			OT_PJ_ATTENDU = oT_PJ_ATTENDU;
		}


		public String getOT_ETAT_GPC() {
			return OT_ETAT_GPC;
		}


		public void setOT_ETAT_GPC(String oT_ETAT_GPC) {
			OT_ETAT_GPC = oT_ETAT_GPC;
		}


		public String getOT_MARGE() {
			return OT_MARGE;
		}


		public void setOT_MARGE(String oT_MARGE) {
			OT_MARGE = oT_MARGE;
		}


		public String getOT_SEGMENT_MARCHE() {
			return OT_SEGMENT_MARCHE;
		}


		public void setOT_SEGMENT_MARCHE(String oT_SEGMENT_MARCHE) {
			OT_SEGMENT_MARCHE = oT_SEGMENT_MARCHE;
		}


		public String getUI_ID() {
			return UI_ID;
		}


		public void setUI_ID(String uI_ID) {
			UI_ID = uI_ID;
		}


		public String getOT_ETL_CODE() {
			return OT_ETL_CODE;
		}


		public void setOT_ETL_CODE(String oT_ETL_CODE) {
			OT_ETL_CODE = oT_ETL_CODE;
		}


		public int getOT_ENVOI_ETECH_ID() {
			return OT_ENVOI_ETECH_ID;
		}


		public void setOT_ENVOI_ETECH_ID(int oT_ENVOI_ETECH_ID) {
			OT_ENVOI_ETECH_ID = oT_ENVOI_ETECH_ID;
		}


		public String getOT_COMMENTAIRE_ANNUL() {
			return OT_COMMENTAIRE_ANNUL;
		}


		public void setOT_COMMENTAIRE_ANNUL(String oT_COMMENTAIRE_ANNUL) {
			OT_COMMENTAIRE_ANNUL = oT_COMMENTAIRE_ANNUL;
		}


		public String getOT_CODE_AGENT() {
			return OT_CODE_AGENT;
		}


		public void setOT_CODE_AGENT(String oT_CODE_AGENT) {
			OT_CODE_AGENT = oT_CODE_AGENT;
		}


		public String getOT_IND_OEB() {
			return OT_IND_OEB;
		}


		public void setOT_IND_OEB(String oT_IND_OEB) {
			OT_IND_OEB = oT_IND_OEB;
		}


		public String getOT_ZONE() {
			return OT_ZONE;
		}


		public void setOT_ZONE(String oT_ZONE) {
			OT_ZONE = oT_ZONE;
		}


		public int getOT_REF_OT_GROUPE() {
			return OT_REF_OT_GROUPE;
		}


		public void setOT_REF_OT_GROUPE(int oT_REF_OT_GROUPE) {
			OT_REF_OT_GROUPE = oT_REF_OT_GROUPE;
		}


		public String getOT_CEDSI() {
			return OT_CEDSI;
		}


		public void setOT_CEDSI(String oT_CEDSI) {
			OT_CEDSI = oT_CEDSI;
		}


		public Date getOT_DLR() {
			return OT_DLR;
		}


		public void setOT_DLR(Date oT_DLR) {
			OT_DLR = oT_DLR;
		}


		public String getOT_COMMUNE() {
			return OT_COMMUNE;
		}


		public void setOT_COMMUNE(String oT_COMMUNE) {
			OT_COMMUNE = oT_COMMUNE;
		}


		public String getOT_ORIGINE() {
			return OT_ORIGINE;
		}


		public void setOT_ORIGINE(String oT_ORIGINE) {
			OT_ORIGINE = oT_ORIGINE;
		}


		public int getOT_NB_ESSAI() {
			return OT_NB_ESSAI;
		}


		public void setOT_NB_ESSAI(int oT_NB_ESSAI) {
			OT_NB_ESSAI = oT_NB_ESSAI;
		}


		public String getOT_NUM_CONTACT() {
			return OT_NUM_CONTACT;
		}


		public void setOT_NUM_CONTACT(String oT_NUM_CONTACT) {
			OT_NUM_CONTACT = oT_NUM_CONTACT;
		}


		/*public int getOT_APPEL_CLIENT() {
			return OT_APPEL_CLIENT;
		}


		public void setOT_APPEL_CLIENT(int oT_APPEL_CLIENT) {
			OT_APPEL_CLIENT = oT_APPEL_CLIENT;
		}*/


		public Date getOT_DATE_TC() {
			return OT_DATE_TC;
		}


		public void setOT_DATE_TC(Date oT_DATE_TC) {
			OT_DATE_TC = oT_DATE_TC;
		}


		public String getOT_RESSOURCES() {
			return OT_RESSOURCES;
		}


		public void setOT_RESSOURCES(String oT_RESSOURCES) {
			OT_RESSOURCES = oT_RESSOURCES;
		}


		public Date getOT_DATE_ETAT_TC_PIDI() {
			return OT_DATE_ETAT_TC_PIDI;
		}


		public void setOT_DATE_ETAT_TC_PIDI(Date oT_DATE_ETAT_TC_PIDI) {
			OT_DATE_ETAT_TC_PIDI = oT_DATE_ETAT_TC_PIDI;
		}


		public String getOT_REF_COMMUNE() {
			return OT_REF_COMMUNE;
		}


		public void setOT_REF_COMMUNE(String oT_REF_COMMUNE) {
			OT_REF_COMMUNE = oT_REF_COMMUNE;
		}


		public Date getOT_DATE_PSG_ATTENTE_CLOTURE() {
			return OT_DATE_PSG_ATTENTE_CLOTURE;
		}


		public void setOT_DATE_PSG_ATTENTE_CLOTURE(Date oT_DATE_PSG_ATTENTE_CLOTURE) {
			OT_DATE_PSG_ATTENTE_CLOTURE = oT_DATE_PSG_ATTENTE_CLOTURE;
		}


		public int getOT_NB_PJ_GEDAFFAIRE() {
			return OT_NB_PJ_GEDAFFAIRE;
		}


		public void setOT_NB_PJ_GEDAFFAIRE(int oT_NB_PJ_GEDAFFAIRE) {
			OT_NB_PJ_GEDAFFAIRE = oT_NB_PJ_GEDAFFAIRE;
		}


		public Date getOT_DATE_RECEPT_PJ_GED() {
			return OT_DATE_RECEPT_PJ_GED;
		}


		public void setOT_DATE_RECEPT_PJ_GED(Date oT_DATE_RECEPT_PJ_GED) {
			OT_DATE_RECEPT_PJ_GED = oT_DATE_RECEPT_PJ_GED;
		}


		public Date getOT_DATE_CONSULT_PJ_GED() {
			return OT_DATE_CONSULT_PJ_GED;
		}


		public void setOT_DATE_CONSULT_PJ_GED(Date oT_DATE_CONSULT_PJ_GED) {
			OT_DATE_CONSULT_PJ_GED = oT_DATE_CONSULT_PJ_GED;
		}


		public String getOT_GDP_CODE() {
			return OT_GDP_CODE;
		}


		public void setOT_GDP_CODE(String oT_GDP_CODE) {
			OT_GDP_CODE = oT_GDP_CODE;
		}


		public int getOT_DFI_ID() {
			return OT_DFI_ID;
		}


		public void setOT_DFI_ID(int oT_DFI_ID) {
			OT_DFI_ID = oT_DFI_ID;
		}


		public String getOT_DEGROUPAGE() {
			return OT_DEGROUPAGE;
		}


		public void setOT_DEGROUPAGE(String oT_DEGROUPAGE) {
			OT_DEGROUPAGE = oT_DEGROUPAGE;
		}


		public int getOT_HAS_DEVIS_FI() {
			return OT_HAS_DEVIS_FI;
		}


		public void setOT_HAS_DEVIS_FI(int oT_HAS_DEVIS_FI) {
			OT_HAS_DEVIS_FI = oT_HAS_DEVIS_FI;
		}


		public int getOT_PJGED_DEMANDE() {
			return OT_PJGED_DEMANDE;
		}


		public void setOT_PJGED_DEMANDE(int oT_PJGED_DEMANDE) {
			OT_PJGED_DEMANDE = oT_PJGED_DEMANDE;
		}


		public int getOT_ETAT_GDP_ID() {
			return OT_ETAT_GDP_ID;
		}


		public void setOT_ETAT_GDP_ID(int oT_ETAT_GDP_ID) {
			OT_ETAT_GDP_ID = oT_ETAT_GDP_ID;
		}


		public Date getOT_DATE_METIER_TA_PIDI() {
			return OT_DATE_METIER_TA_PIDI;
		}


		public void setOT_DATE_METIER_TA_PIDI(Date oT_DATE_METIER_TA_PIDI) {
			OT_DATE_METIER_TA_PIDI = oT_DATE_METIER_TA_PIDI;
		}


		public Date getOT_DATE_SYSTEM_TR_PIDI() {
			return OT_DATE_SYSTEM_TR_PIDI;
		}


		public void setOT_DATE_SYSTEM_TR_PIDI(Date oT_DATE_SYSTEM_TR_PIDI) {
			OT_DATE_SYSTEM_TR_PIDI = oT_DATE_SYSTEM_TR_PIDI;
		}


		public Date getOT_DATE_SYSTEM_TC2_PIDI() {
			return OT_DATE_SYSTEM_TC2_PIDI;
		}


		public void setOT_DATE_SYSTEM_TC2_PIDI(Date oT_DATE_SYSTEM_TC2_PIDI) {
			OT_DATE_SYSTEM_TC2_PIDI = oT_DATE_SYSTEM_TC2_PIDI;
		}


		public Date getOT_DATE_METIER_TC2_PIDI() {
			return OT_DATE_METIER_TC2_PIDI;
		}


		public void setOT_DATE_METIER_TC2_PIDI(Date oT_DATE_METIER_TC2_PIDI) {
			OT_DATE_METIER_TC2_PIDI = oT_DATE_METIER_TC2_PIDI;
		}


		public Date getOT_DATE_REPLANIFICATION_TR() {
			return OT_DATE_REPLANIFICATION_TR;
		}


		public void setOT_DATE_REPLANIFICATION_TR(Date oT_DATE_REPLANIFICATION_TR) {
			OT_DATE_REPLANIFICATION_TR = oT_DATE_REPLANIFICATION_TR;
		}


		public int getOT_INDIC_RDV() {
			return OT_INDIC_RDV;
		}


		public void setOT_INDIC_RDV(int oT_INDIC_RDV) {
			OT_INDIC_RDV = oT_INDIC_RDV;
		}


		public Date getOT_DATE_RECEPT_MODIF_SI() {
			return OT_DATE_RECEPT_MODIF_SI;
		}


		public void setOT_DATE_RECEPT_MODIF_SI(Date oT_DATE_RECEPT_MODIF_SI) {
			OT_DATE_RECEPT_MODIF_SI = oT_DATE_RECEPT_MODIF_SI;
		}


		public String getOT_NUM_VOIE() {
			return OT_NUM_VOIE;
		}


		public void setOT_NUM_VOIE(String oT_NUM_VOIE) {
			OT_NUM_VOIE = oT_NUM_VOIE;
		}


		public String getOT_LIBELLE_VOIE() {
			return OT_LIBELLE_VOIE;
		}


		public void setOT_LIBELLE_VOIE(String oT_LIBELLE_VOIE) {
			OT_LIBELLE_VOIE = oT_LIBELLE_VOIE;
		}


		public int getOT_ENVOYER_ATON() {
			return OT_ENVOYER_ATON;
		}


		public void setOT_ENVOYER_ATON(int oT_ENVOYER_ATON) {
			OT_ENVOYER_ATON = oT_ENVOYER_ATON;
		}


		public Date getOT_DATE_REPLANIFICATION() {
			return OT_DATE_REPLANIFICATION;
		}


		public void setOT_DATE_REPLANIFICATION(Date oT_DATE_REPLANIFICATION) {
			OT_DATE_REPLANIFICATION = oT_DATE_REPLANIFICATION;
		}


		public int getOT_ENVOYER_ETL() {
			return OT_ENVOYER_ETL;
		}


		public void setOT_ENVOYER_ETL(int oT_ENVOYER_ETL) {
			OT_ENVOYER_ETL = oT_ENVOYER_ETL;
		}


		public String getOT_COMMENTAIRE_SUIVI() {
			return OT_COMMENTAIRE_SUIVI;
		}


		public void setOT_COMMENTAIRE_SUIVI(String oT_COMMENTAIRE_SUIVI) {
			OT_COMMENTAIRE_SUIVI = oT_COMMENTAIRE_SUIVI;
		}


		public String getOT_TYPE_DEBIT() {
			return OT_TYPE_DEBIT;
		}


		public void setOT_TYPE_DEBIT(String oT_TYPE_DEBIT) {
			OT_TYPE_DEBIT = oT_TYPE_DEBIT;
		}


		public String getOT_POINT_COUPURE() {
			return OT_POINT_COUPURE;
		}


		public void setOT_POINT_COUPURE(String oT_POINT_COUPURE) {
			OT_POINT_COUPURE = oT_POINT_COUPURE;
		}


		public String getOT_TYPE_REGROUPEMENT() {
			return OT_TYPE_REGROUPEMENT;
		}


		public void setOT_TYPE_REGROUPEMENT(String oT_TYPE_REGROUPEMENT) {
			OT_TYPE_REGROUPEMENT = oT_TYPE_REGROUPEMENT;
		}


		public String getOT_OPERATEUR() {
			return OT_OPERATEUR;
		}


		public void setOT_OPERATEUR(String oT_OPERATEUR) {
			OT_OPERATEUR = oT_OPERATEUR;
		}


		public String getOT_NUMPRESTACLIP() {
			return OT_NUMPRESTACLIP;
		}


		public void setOT_NUMPRESTACLIP(String oT_NUMPRESTACLIP) {
			OT_NUMPRESTACLIP = oT_NUMPRESTACLIP;
		}


		public Date getOT_DATE_ANNONCEE() {
			return OT_DATE_ANNONCEE;
		}


		public void setOT_DATE_ANNONCEE(Date oT_DATE_ANNONCEE) {
			OT_DATE_ANNONCEE = oT_DATE_ANNONCEE;
		}


		/*public int getOT_CHANTIER_ID() {
			return OT_CHANTIER_ID;
		}


		public void setOT_CHANTIER_ID(int oT_CHANTIER_ID) {
			OT_CHANTIER_ID = oT_CHANTIER_ID;
		}*/


		public Date getOT_DATE_ECHANTILLON() {
			return OT_DATE_ECHANTILLON;
		}


		public void setOT_DATE_ECHANTILLON(Date oT_DATE_ECHANTILLON) {
			OT_DATE_ECHANTILLON = oT_DATE_ECHANTILLON;
		}


		public String getOT_DTIO() {
			return OT_DTIO;
		}


		public void setOT_DTIO(String oT_DTIO) {
			OT_DTIO = oT_DTIO;
		}


		public String getOT_CATEGORIE_PLP() {
			return OT_CATEGORIE_PLP;
		}


		public void setOT_CATEGORIE_PLP(String oT_CATEGORIE_PLP) {
			OT_CATEGORIE_PLP = oT_CATEGORIE_PLP;
		}


		public String getOT_NUM_PTO() {
			return OT_NUM_PTO;
		}


		public void setOT_NUM_PTO(String oT_NUM_PTO) {
			OT_NUM_PTO = oT_NUM_PTO;
		}


		public String getOT_NUM_PORTE_CLIENT() {
			return OT_NUM_PORTE_CLIENT;
		}


		public void setOT_NUM_PORTE_CLIENT(String oT_NUM_PORTE_CLIENT) {
			OT_NUM_PORTE_CLIENT = oT_NUM_PORTE_CLIENT;
		}


		public int getOT_SDSL() {
			return OT_SDSL;
		}


		public void setOT_SDSL(int oT_SDSL) {
			OT_SDSL = oT_SDSL;
		}


		public String getOT_INDIC_COMPLEX() {
			return OT_INDIC_COMPLEX;
		}


		public void setOT_INDIC_COMPLEX(String oT_INDIC_COMPLEX) {
			OT_INDIC_COMPLEX = oT_INDIC_COMPLEX;
		}


		public String getOT_BLOCNOTE() {
			return OT_BLOCNOTE;
		}


		public void setOT_BLOCNOTE(String oT_BLOCNOTE) {
			OT_BLOCNOTE = oT_BLOCNOTE;
		}


		public String getOT_TAG_PROCESS_CONTEXT() {
			return OT_TAG_PROCESS_CONTEXT;
		}


		public void setOT_TAG_PROCESS_CONTEXT(String oT_TAG_PROCESS_CONTEXT) {
			OT_TAG_PROCESS_CONTEXT = oT_TAG_PROCESS_CONTEXT;
		}


		public String getOT_REAFFECTE() {
			return OT_REAFFECTE;
		}


		public void setOT_REAFFECTE(String oT_REAFFECTE) {
			OT_REAFFECTE = oT_REAFFECTE;
		}


		public int getOT_NB_TC() {
			return OT_NB_TC;
		}


		public void setOT_NB_TC(int oT_NB_TC) {
			OT_NB_TC = oT_NB_TC;
		}


		public Set<ETL> getEtl() {
			return etl;
		}


		public void setEtl(Set<ETL> etl) {
			this.etl = etl;
		}


		public Set<UI> getUi() {
			return ui;
		}


		public void setUi(Set<UI> ui) {
			this.ui = ui;
		}
	
		
}
