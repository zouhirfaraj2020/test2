package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CRI {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int CRI_ID;
		private int OT_ID; 
		private Date CRI_DATE_CLOTURE;
		
		public CRI(int cRI_ID, int oT_ID, Date cRI_DATE_CLOTURE) {
			super();
			CRI_ID = cRI_ID;
			OT_ID = oT_ID;
			CRI_DATE_CLOTURE = cRI_DATE_CLOTURE;
		}
		
		public CRI() {
			super();
		}

		public int getCRI_ID() {
			return CRI_ID;
		}

		public void setCRI_ID(int cRI_ID) {
			CRI_ID = cRI_ID;
		}

		public int getOT_ID() {
			return OT_ID;
		}

		public void setOT_ID(int oT_ID) {
			OT_ID = oT_ID;
		}

		public Date getCRI_DATE_CLOTURE() {
			return CRI_DATE_CLOTURE;
		}

		public void setCRI_DATE_CLOTURE(Date cRI_DATE_CLOTURE) {
			CRI_DATE_CLOTURE = cRI_DATE_CLOTURE;
		}

		@Override
		public String toString() {
			return "CRI [CRI_ID=" + CRI_ID + ", OT_ID=" + OT_ID + ", CRI_DATE_CLOTURE=" + CRI_DATE_CLOTURE + "]";
		}
		
		

}
