package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Null;
@Entity
@Table(name="GDP_ATT_MONTANT")
public class GDP_ATT_MONTANT {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int GDP_ATT_MONTANT_ID; 
		private Date GDP_ATT_DATE_ACQUIT_REJET_GDP;
		private Date GDP_ATT_DATE_ACQUIT_REJET_RQF ;
		private Date GDP_ATT_DATE_VALIDATION;
		private Date GDP_ATT_DATE_ENVOI_GDP;
		private Date GDP_ATT_DATE_CREATION;
		@Null
		@Column(name = "GDP_ATT_ETAT_ID")
		private int GDP_ATT_ETAT_ID;
	
		public GDP_ATT_MONTANT() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		

		public GDP_ATT_MONTANT(int gDP_ATT_MONTANT_ID, Date gDP_ATT_DATE_ACQUIT_REJET_GDP,
				Date gDP_ATT_DATE_ACQUIT_REJET_RQF, Date gDP_ATT_DATE_VALIDATION, Date gDP_ATT_DATE_ENVOI_GDP,
				Date gDP_ATT_DATE_CREATION, @Null int gDP_ATT_ETAT_ID) {
			super();
			GDP_ATT_MONTANT_ID = gDP_ATT_MONTANT_ID;
			GDP_ATT_DATE_ACQUIT_REJET_GDP = gDP_ATT_DATE_ACQUIT_REJET_GDP;
			GDP_ATT_DATE_ACQUIT_REJET_RQF = gDP_ATT_DATE_ACQUIT_REJET_RQF;
			GDP_ATT_DATE_VALIDATION = gDP_ATT_DATE_VALIDATION;
			GDP_ATT_DATE_ENVOI_GDP = gDP_ATT_DATE_ENVOI_GDP;
			GDP_ATT_DATE_CREATION = gDP_ATT_DATE_CREATION;
			GDP_ATT_ETAT_ID = gDP_ATT_ETAT_ID;
		}



		public int getGDP_ATT_MONTANT_ID() {
			return GDP_ATT_MONTANT_ID;
		}

		public void setGDP_ATT_MONTANT_ID(int gDP_ATT_MONTANT_ID) {
			GDP_ATT_MONTANT_ID = gDP_ATT_MONTANT_ID;
		}

		public int getGDP_ATT_ETAT_ID() {
			return GDP_ATT_ETAT_ID;
		}

		public void setGDP_ATT_ETAT_ID(int gDP_ATT_ETAT_ID) {
			GDP_ATT_ETAT_ID = gDP_ATT_ETAT_ID;
		}

		public Date getGDP_ATT_DATE_ACQUIT_REJET_GDP() {
			return GDP_ATT_DATE_ACQUIT_REJET_GDP;
		}

		public void setGDP_ATT_DATE_ACQUIT_REJET_GDP(Date gDP_ATT_DATE_ACQUIT_REJET_GDP) {
			GDP_ATT_DATE_ACQUIT_REJET_GDP = gDP_ATT_DATE_ACQUIT_REJET_GDP;
		}

		public Date getGDP_ATT_DATE_ACQUIT_REJET_RQF() {
			return GDP_ATT_DATE_ACQUIT_REJET_RQF;
		}

		public void setGDP_ATT_DATE_ACQUIT_REJET_RQF(Date gDP_ATT_DATE_ACQUIT_REJET_RQF) {
			GDP_ATT_DATE_ACQUIT_REJET_RQF = gDP_ATT_DATE_ACQUIT_REJET_RQF;
		}

		public Date getGDP_ATT_DATE_VALIDATION() {
			return GDP_ATT_DATE_VALIDATION;
		}

		public void setGDP_ATT_DATE_VALIDATION(Date gDP_ATT_DATE_VALIDATION) {
			GDP_ATT_DATE_VALIDATION = gDP_ATT_DATE_VALIDATION;
		}

		public Date getGDP_ATT_DATE_ENVOI_GDP() {
			return GDP_ATT_DATE_ENVOI_GDP;
		}

		public void setGDP_ATT_DATE_ENVOI_GDP(Date gDP_ATT_DATE_ENVOI_GDP) {
			GDP_ATT_DATE_ENVOI_GDP = gDP_ATT_DATE_ENVOI_GDP;
		}

		public Date getGDP_ATT_DATE_CREATION() {
			return GDP_ATT_DATE_CREATION;
		}

		public void setGDP_ATT_DATE_CREATION(Date gDP_ATT_DATE_CREATION) {
			GDP_ATT_DATE_CREATION = gDP_ATT_DATE_CREATION;
		}

		
		
}
