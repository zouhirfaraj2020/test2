package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Null;

@Entity
@Table(name="ACTIVITE")
public class ACTIVITE {

			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)
			private int ACT_ID; 
			private String ACT_CODE; 
			@Null
			@Column(name = "DOM_CODE")
			private int DOM_CODE; 
			private String ACT_LIBELLE; 
			private Date ACT_DATE_CREATION;
			
			public ACTIVITE() {
				super();
				// TODO Auto-generated constructor stub
			}
			public ACTIVITE(int aCT_ID, String aCT_CODE, @Null int dOM_CODE, String aCT_LIBELLE,
					Date aCT_DATE_CREATION) {
				super();
				ACT_ID = aCT_ID;
				ACT_CODE = aCT_CODE;
				DOM_CODE = dOM_CODE;
				ACT_LIBELLE = aCT_LIBELLE;
				ACT_DATE_CREATION = aCT_DATE_CREATION;
			}
			public int getACT_ID() {
				return ACT_ID;
			}
			public void setACT_ID(int aCT_ID) {
				ACT_ID = aCT_ID;
			}
			public String getACT_CODE() {
				return ACT_CODE;
			}
			public void setACT_CODE(String aCT_CODE) {
				ACT_CODE = aCT_CODE;
			}
			public int getDOM_CODE() {
				return DOM_CODE;
			}
			public void setDOM_CODE(int dOM_CODE) {
				DOM_CODE = dOM_CODE;
			}
			public String getACT_LIBELLE() {
				return ACT_LIBELLE;
			}
			public void setACT_LIBELLE(String aCT_LIBELLE) {
				ACT_LIBELLE = aCT_LIBELLE;
			}
			public Date getACT_DATE_CREATION() {
				return ACT_DATE_CREATION;
			}
			public void setACT_DATE_CREATION(Date aCT_DATE_CREATION) {
				ACT_DATE_CREATION = aCT_DATE_CREATION;
			} 
			
			
}
