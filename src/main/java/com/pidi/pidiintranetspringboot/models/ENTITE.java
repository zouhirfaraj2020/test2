package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Null;

@Entity
@Table(name="ENTITE")

public class ENTITE {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int ENT_ID; 
	private String ENT_CODE; 
	private String ENT_LIBELLE;

	private String GPC_CODE;
	private Date ENT_DATE_CREATION;
	private Date ENT_DATE_MODIFICATION;
	private String ENT_ADRESSE;
	private String ENT_MAIL;
	private String ENT_FAX;
	@Null
	@Column(name = "ENT_CLIENT_AVISE")
	private int ENT_CLIENT_AVISE;
	@Null
	@Column(name = "ENT_TYPE")
	private int ENT_TYPE;
	@Null
	@Column(name = "ENT_ENVOI_ETECH_ACTIF")
	private int ENT_ENVOI_ETECH_ACTIF;
	private String VERSION_PIDI;
	private String ENT_ADRESSE_REPLI;
	@Null
	@Column(name = "ENT_DEBRAYAGE_CONTROLE_ESSAI")
	private int ENT_DEBRAYAGE_CONTROLE_ESSAI;
	@Null
	@Column(name = "ENT_DEBRAYAGE_CHANGEMENT_ETAT")
	private int ENT_DEBRAYAGE_CHANGEMENT_ETAT;
	private String ENT_ADRESSE_REPLI2;
	@Null
	@Column(name = "ENT_DEBRAYAGE_GED")
	private int ENT_DEBRAYAGE_GED;
	@Null
	@Column(name = "ENT_DELAI_RAPPEL_RQF")
	private int ENT_DELAI_RAPPEL_RQF;
	private String URL_CONNEXION;
	@Null
	@Column(name = "ACTIV_AUTRETETE_ENT")
	private int ACTIV_AUTRETETE_ENT;
	private String ENT_MAIL_ALERTE_TAM;
	private String ENT_ADRESSE_POSTALE;
	private String ENT_MAIL_CONTACT_DALEMBERT;
	private String ENT_CUID_MAIL_BO_DALEMBERT;
	private String ENT_MAIL_FORMULAIRE;
	
	public ENTITE() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ENTITE(int eNT_ID, String eNT_CODE, String eNT_LIBELLE, String gPC_CODE, Date eNT_DATE_CREATION,
			Date eNT_DATE_MODIFICATION, String eNT_ADRESSE, String eNT_MAIL, String eNT_FAX, @Null int eNT_CLIENT_AVISE,
			@Null int eNT_TYPE, @Null int eNT_ENVOI_ETECH_ACTIF, String vERSION_PIDI, String eNT_ADRESSE_REPLI,
			@Null int eNT_DEBRAYAGE_CONTROLE_ESSAI, @Null int eNT_DEBRAYAGE_CHANGEMENT_ETAT, String eNT_ADRESSE_REPLI2,
			@Null int eNT_DEBRAYAGE_GED, @Null int eNT_DELAI_RAPPEL_RQF, String uRL_CONNEXION,
			@Null int aCTIV_AUTRETETE_ENT, String eNT_MAIL_ALERTE_TAM, String eNT_ADRESSE_POSTALE,
			String eNT_MAIL_CONTACT_DALEMBERT, String eNT_CUID_MAIL_BO_DALEMBERT, String eNT_MAIL_FORMULAIRE) {
		super();
		ENT_ID = eNT_ID;
		ENT_CODE = eNT_CODE;
		ENT_LIBELLE = eNT_LIBELLE;
		GPC_CODE = gPC_CODE;
		ENT_DATE_CREATION = eNT_DATE_CREATION;
		ENT_DATE_MODIFICATION = eNT_DATE_MODIFICATION;
		ENT_ADRESSE = eNT_ADRESSE;
		ENT_MAIL = eNT_MAIL;
		ENT_FAX = eNT_FAX;
		ENT_CLIENT_AVISE = eNT_CLIENT_AVISE;
		ENT_TYPE = eNT_TYPE;
		ENT_ENVOI_ETECH_ACTIF = eNT_ENVOI_ETECH_ACTIF;
		VERSION_PIDI = vERSION_PIDI;
		ENT_ADRESSE_REPLI = eNT_ADRESSE_REPLI;
		ENT_DEBRAYAGE_CONTROLE_ESSAI = eNT_DEBRAYAGE_CONTROLE_ESSAI;
		ENT_DEBRAYAGE_CHANGEMENT_ETAT = eNT_DEBRAYAGE_CHANGEMENT_ETAT;
		ENT_ADRESSE_REPLI2 = eNT_ADRESSE_REPLI2;
		ENT_DEBRAYAGE_GED = eNT_DEBRAYAGE_GED;
		ENT_DELAI_RAPPEL_RQF = eNT_DELAI_RAPPEL_RQF;
		URL_CONNEXION = uRL_CONNEXION;
		ACTIV_AUTRETETE_ENT = aCTIV_AUTRETETE_ENT;
		ENT_MAIL_ALERTE_TAM = eNT_MAIL_ALERTE_TAM;
		ENT_ADRESSE_POSTALE = eNT_ADRESSE_POSTALE;
		ENT_MAIL_CONTACT_DALEMBERT = eNT_MAIL_CONTACT_DALEMBERT;
		ENT_CUID_MAIL_BO_DALEMBERT = eNT_CUID_MAIL_BO_DALEMBERT;
		ENT_MAIL_FORMULAIRE = eNT_MAIL_FORMULAIRE;
	}

	public int getENT_ID() {
		return ENT_ID;
	}

	public void setENT_ID(int eNT_ID) {
		ENT_ID = eNT_ID;
	}

	public String getENT_CODE() {
		return ENT_CODE;
	}

	public void setENT_CODE(String eNT_CODE) {
		ENT_CODE = eNT_CODE;
	}

	public String getENT_LIBELLE() {
		return ENT_LIBELLE;
	}

	public void setENT_LIBELLE(String eNT_LIBELLE) {
		ENT_LIBELLE = eNT_LIBELLE;
	}

	public String getGPC_CODE() {
		return GPC_CODE;
	}

	public void setGPC_CODE(String gPC_CODE) {
		GPC_CODE = gPC_CODE;
	}

	public Date getENT_DATE_CREATION() {
		return ENT_DATE_CREATION;
	}

	public void setENT_DATE_CREATION(Date eNT_DATE_CREATION) {
		ENT_DATE_CREATION = eNT_DATE_CREATION;
	}

	public Date getENT_DATE_MODIFICATION() {
		return ENT_DATE_MODIFICATION;
	}

	public void setENT_DATE_MODIFICATION(Date eNT_DATE_MODIFICATION) {
		ENT_DATE_MODIFICATION = eNT_DATE_MODIFICATION;
	}

	public String getENT_ADRESSE() {
		return ENT_ADRESSE;
	}

	public void setENT_ADRESSE(String eNT_ADRESSE) {
		ENT_ADRESSE = eNT_ADRESSE;
	}

	public String getENT_MAIL() {
		return ENT_MAIL;
	}

	public void setENT_MAIL(String eNT_MAIL) {
		ENT_MAIL = eNT_MAIL;
	}

	public String getENT_FAX() {
		return ENT_FAX;
	}

	public void setENT_FAX(String eNT_FAX) {
		ENT_FAX = eNT_FAX;
	}

	public int getENT_CLIENT_AVISE() {
		return ENT_CLIENT_AVISE;
	}

	public void setENT_CLIENT_AVISE(int eNT_CLIENT_AVISE) {
		ENT_CLIENT_AVISE = eNT_CLIENT_AVISE;
	}

	public int getENT_TYPE() {
		return ENT_TYPE;
	}

	public void setENT_TYPE(int eNT_TYPE) {
		ENT_TYPE = eNT_TYPE;
	}

	public int getENT_ENVOI_ETECH_ACTIF() {
		return ENT_ENVOI_ETECH_ACTIF;
	}

	public void setENT_ENVOI_ETECH_ACTIF(int eNT_ENVOI_ETECH_ACTIF) {
		ENT_ENVOI_ETECH_ACTIF = eNT_ENVOI_ETECH_ACTIF;
	}

	public String getVERSION_PIDI() {
		return VERSION_PIDI;
	}

	public void setVERSION_PIDI(String vERSION_PIDI) {
		VERSION_PIDI = vERSION_PIDI;
	}

	public String getENT_ADRESSE_REPLI() {
		return ENT_ADRESSE_REPLI;
	}

	public void setENT_ADRESSE_REPLI(String eNT_ADRESSE_REPLI) {
		ENT_ADRESSE_REPLI = eNT_ADRESSE_REPLI;
	}

	public int getENT_DEBRAYAGE_CONTROLE_ESSAI() {
		return ENT_DEBRAYAGE_CONTROLE_ESSAI;
	}

	public void setENT_DEBRAYAGE_CONTROLE_ESSAI(int eNT_DEBRAYAGE_CONTROLE_ESSAI) {
		ENT_DEBRAYAGE_CONTROLE_ESSAI = eNT_DEBRAYAGE_CONTROLE_ESSAI;
	}

	public int getENT_DEBRAYAGE_CHANGEMENT_ETAT() {
		return ENT_DEBRAYAGE_CHANGEMENT_ETAT;
	}

	public void setENT_DEBRAYAGE_CHANGEMENT_ETAT(int eNT_DEBRAYAGE_CHANGEMENT_ETAT) {
		ENT_DEBRAYAGE_CHANGEMENT_ETAT = eNT_DEBRAYAGE_CHANGEMENT_ETAT;
	}

	public String getENT_ADRESSE_REPLI2() {
		return ENT_ADRESSE_REPLI2;
	}

	public void setENT_ADRESSE_REPLI2(String eNT_ADRESSE_REPLI2) {
		ENT_ADRESSE_REPLI2 = eNT_ADRESSE_REPLI2;
	}

	public int getENT_DEBRAYAGE_GED() {
		return ENT_DEBRAYAGE_GED;
	}

	public void setENT_DEBRAYAGE_GED(int eNT_DEBRAYAGE_GED) {
		ENT_DEBRAYAGE_GED = eNT_DEBRAYAGE_GED;
	}

	public int getENT_DELAI_RAPPEL_RQF() {
		return ENT_DELAI_RAPPEL_RQF;
	}

	public void setENT_DELAI_RAPPEL_RQF(int eNT_DELAI_RAPPEL_RQF) {
		ENT_DELAI_RAPPEL_RQF = eNT_DELAI_RAPPEL_RQF;
	}

	public String getURL_CONNEXION() {
		return URL_CONNEXION;
	}

	public void setURL_CONNEXION(String uRL_CONNEXION) {
		URL_CONNEXION = uRL_CONNEXION;
	}

	public int getACTIV_AUTRETETE_ENT() {
		return ACTIV_AUTRETETE_ENT;
	}

	public void setACTIV_AUTRETETE_ENT(int aCTIV_AUTRETETE_ENT) {
		ACTIV_AUTRETETE_ENT = aCTIV_AUTRETETE_ENT;
	}

	public String getENT_MAIL_ALERTE_TAM() {
		return ENT_MAIL_ALERTE_TAM;
	}

	public void setENT_MAIL_ALERTE_TAM(String eNT_MAIL_ALERTE_TAM) {
		ENT_MAIL_ALERTE_TAM = eNT_MAIL_ALERTE_TAM;
	}

	public String getENT_ADRESSE_POSTALE() {
		return ENT_ADRESSE_POSTALE;
	}

	public void setENT_ADRESSE_POSTALE(String eNT_ADRESSE_POSTALE) {
		ENT_ADRESSE_POSTALE = eNT_ADRESSE_POSTALE;
	}

	public String getENT_MAIL_CONTACT_DALEMBERT() {
		return ENT_MAIL_CONTACT_DALEMBERT;
	}

	public void setENT_MAIL_CONTACT_DALEMBERT(String eNT_MAIL_CONTACT_DALEMBERT) {
		ENT_MAIL_CONTACT_DALEMBERT = eNT_MAIL_CONTACT_DALEMBERT;
	}

	public String getENT_CUID_MAIL_BO_DALEMBERT() {
		return ENT_CUID_MAIL_BO_DALEMBERT;
	}

	public void setENT_CUID_MAIL_BO_DALEMBERT(String eNT_CUID_MAIL_BO_DALEMBERT) {
		ENT_CUID_MAIL_BO_DALEMBERT = eNT_CUID_MAIL_BO_DALEMBERT;
	}

	public String getENT_MAIL_FORMULAIRE() {
		return ENT_MAIL_FORMULAIRE;
	}

	public void setENT_MAIL_FORMULAIRE(String eNT_MAIL_FORMULAIRE) {
		ENT_MAIL_FORMULAIRE = eNT_MAIL_FORMULAIRE;
	}
	
	
	

}
