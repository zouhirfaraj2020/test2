package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

public class ChartModel {

	private int nbr;
	private Date dateEta;
	
	
	public ChartModel(int nbr, Date dateEta) {
		super();
		this.nbr = nbr;
		this.dateEta = dateEta;
	}
	
	public ChartModel() {
		super();
	}
	
	public int getNbr() {
		return nbr;
	}
	public void setNbr(int nbr) {
		this.nbr = nbr;
	}
	public Date getDateEta() {
		return dateEta;
	}
	public void setDateEta(Date dateEta) {
		this.dateEta = dateEta;
	}

	@Override
	public String toString() {
		return "ChartModel [nbr=" + nbr + ", dateEta=" + dateEta + "]";
	}
	
	
}
