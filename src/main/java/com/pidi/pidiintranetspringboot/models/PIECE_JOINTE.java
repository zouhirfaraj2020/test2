package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Null;

@Entity
@Table(name="PIECE_JOINTE")
public class PIECE_JOINTE {
  
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int PJO_ID; 
		private String TYPE_PJ_ID; 
		/*@Null
		@Column(name = "OT_ID")*/
		private Integer OT_ID; 
		/*@Null
		@Column(name = "CRI_ID")*/
		private Integer CRI_ID; 
		private String PJO_COMMENTAIRE; 
		
		private String PJO_NOM; 
		private String PJO_BIN; 
		private Date PJO_DATE_INSERTION; 
		private Date PJO_DATE_ENVOI_SI; 
		/*@Null
		@Column(name = "PJO_GEDAFFAIRE")*/
		private Integer PJO_GEDAFFAIRE; 
		/*@Null
		@Column(name = "PJO_GED_ENVOI_M03")*/
		private Integer PJO_GED_ENVOI_M03; 
		private String PJO_GED_ACTION; 
		private Date PJO_GED_DATE_ENVOI_M03; 
		private Date PJO_DATE_RECEPTION_SI; 
		/*@Null
		@Column(name = "PJO_MODE_ENVOI_SI")*/
		private Integer PJO_MODE_ENVOI_SI; 
		/*@Null
		@Column(name = "PJO_ETAT_ENVOI_GED_OPTIMUM")*/
		private Integer PJO_ETAT_ENVOI_GED_OPTIMUM; 
		private Date PJO_DATE_ENVOI_GED_OPTIMUM;
		public PIECE_JOINTE(int pJO_ID, String tYPE_PJ_ID, Integer oT_ID, Integer cRI_ID, String pJO_COMMENTAIRE,
				String pJO_NOM, String pJO_BIN, Date pJO_DATE_INSERTION, Date pJO_DATE_ENVOI_SI, Integer pJO_GEDAFFAIRE,
				Integer pJO_GED_ENVOI_M03, String pJO_GED_ACTION, Date pJO_GED_DATE_ENVOI_M03,
				Date pJO_DATE_RECEPTION_SI, Integer pJO_MODE_ENVOI_SI, Integer pJO_ETAT_ENVOI_GED_OPTIMUM,
				Date pJO_DATE_ENVOI_GED_OPTIMUM) {
			super();
			PJO_ID = pJO_ID;
			TYPE_PJ_ID = tYPE_PJ_ID;
			OT_ID = oT_ID;
			CRI_ID = cRI_ID;
			PJO_COMMENTAIRE = pJO_COMMENTAIRE;
			PJO_NOM = pJO_NOM;
			PJO_BIN = pJO_BIN;
			PJO_DATE_INSERTION = pJO_DATE_INSERTION;
			PJO_DATE_ENVOI_SI = pJO_DATE_ENVOI_SI;
			PJO_GEDAFFAIRE = pJO_GEDAFFAIRE;
			PJO_GED_ENVOI_M03 = pJO_GED_ENVOI_M03;
			PJO_GED_ACTION = pJO_GED_ACTION;
			PJO_GED_DATE_ENVOI_M03 = pJO_GED_DATE_ENVOI_M03;
			PJO_DATE_RECEPTION_SI = pJO_DATE_RECEPTION_SI;
			PJO_MODE_ENVOI_SI = pJO_MODE_ENVOI_SI;
			PJO_ETAT_ENVOI_GED_OPTIMUM = pJO_ETAT_ENVOI_GED_OPTIMUM;
			PJO_DATE_ENVOI_GED_OPTIMUM = pJO_DATE_ENVOI_GED_OPTIMUM;
		}
		public PIECE_JOINTE() {
			super();
			// TODO Auto-generated constructor stub
		}
		public int getPJO_ID() {
			return PJO_ID;
		}
		public void setPJO_ID(int pJO_ID) {
			PJO_ID = pJO_ID;
		}
		public String getTYPE_PJ_ID() {
			return TYPE_PJ_ID;
		}
		public void setTYPE_PJ_ID(String tYPE_PJ_ID) {
			TYPE_PJ_ID = tYPE_PJ_ID;
		}
		public Integer getOT_ID() {
			return OT_ID;
		}
		public void setOT_ID(Integer oT_ID) {
			OT_ID = oT_ID;
		}
		public Integer getCRI_ID() {
			return CRI_ID;
		}
		public void setCRI_ID(Integer cRI_ID) {
			CRI_ID = cRI_ID;
		}
		public String getPJO_COMMENTAIRE() {
			return PJO_COMMENTAIRE;
		}
		public void setPJO_COMMENTAIRE(String pJO_COMMENTAIRE) {
			PJO_COMMENTAIRE = pJO_COMMENTAIRE;
		}
		public String getPJO_NOM() {
			return PJO_NOM;
		}
		public void setPJO_NOM(String pJO_NOM) {
			PJO_NOM = pJO_NOM;
		}
		public String getPJO_BIN() {
			return PJO_BIN;
		}
		public void setPJO_BIN(String pJO_BIN) {
			PJO_BIN = pJO_BIN;
		}
		public Date getPJO_DATE_INSERTION() {
			return PJO_DATE_INSERTION;
		}
		public void setPJO_DATE_INSERTION(Date pJO_DATE_INSERTION) {
			PJO_DATE_INSERTION = pJO_DATE_INSERTION;
		}
		public Date getPJO_DATE_ENVOI_SI() {
			return PJO_DATE_ENVOI_SI;
		}
		public void setPJO_DATE_ENVOI_SI(Date pJO_DATE_ENVOI_SI) {
			PJO_DATE_ENVOI_SI = pJO_DATE_ENVOI_SI;
		}
		public Integer getPJO_GEDAFFAIRE() {
			return PJO_GEDAFFAIRE;
		}
		public void setPJO_GEDAFFAIRE(Integer pJO_GEDAFFAIRE) {
			PJO_GEDAFFAIRE = pJO_GEDAFFAIRE;
		}
		public Integer getPJO_GED_ENVOI_M03() {
			return PJO_GED_ENVOI_M03;
		}
		public void setPJO_GED_ENVOI_M03(Integer pJO_GED_ENVOI_M03) {
			PJO_GED_ENVOI_M03 = pJO_GED_ENVOI_M03;
		}
		public String getPJO_GED_ACTION() {
			return PJO_GED_ACTION;
		}
		public void setPJO_GED_ACTION(String pJO_GED_ACTION) {
			PJO_GED_ACTION = pJO_GED_ACTION;
		}
		public Date getPJO_GED_DATE_ENVOI_M03() {
			return PJO_GED_DATE_ENVOI_M03;
		}
		public void setPJO_GED_DATE_ENVOI_M03(Date pJO_GED_DATE_ENVOI_M03) {
			PJO_GED_DATE_ENVOI_M03 = pJO_GED_DATE_ENVOI_M03;
		}
		public Date getPJO_DATE_RECEPTION_SI() {
			return PJO_DATE_RECEPTION_SI;
		}
		public void setPJO_DATE_RECEPTION_SI(Date pJO_DATE_RECEPTION_SI) {
			PJO_DATE_RECEPTION_SI = pJO_DATE_RECEPTION_SI;
		}
		public Integer getPJO_MODE_ENVOI_SI() {
			return PJO_MODE_ENVOI_SI;
		}
		public void setPJO_MODE_ENVOI_SI(Integer pJO_MODE_ENVOI_SI) {
			PJO_MODE_ENVOI_SI = pJO_MODE_ENVOI_SI;
		}
		public Integer getPJO_ETAT_ENVOI_GED_OPTIMUM() {
			return PJO_ETAT_ENVOI_GED_OPTIMUM;
		}
		public void setPJO_ETAT_ENVOI_GED_OPTIMUM(Integer pJO_ETAT_ENVOI_GED_OPTIMUM) {
			PJO_ETAT_ENVOI_GED_OPTIMUM = pJO_ETAT_ENVOI_GED_OPTIMUM;
		}
		public Date getPJO_DATE_ENVOI_GED_OPTIMUM() {
			return PJO_DATE_ENVOI_GED_OPTIMUM;
		}
		public void setPJO_DATE_ENVOI_GED_OPTIMUM(Date pJO_DATE_ENVOI_GED_OPTIMUM) {
			PJO_DATE_ENVOI_GED_OPTIMUM = pJO_DATE_ENVOI_GED_OPTIMUM;
		}
		@Override
		public String toString() {
			return "PIECE_JOINTE [PJO_ID=" + PJO_ID + ", TYPE_PJ_ID=" + TYPE_PJ_ID + ", OT_ID=" + OT_ID + ", CRI_ID="
					+ CRI_ID + ", PJO_COMMENTAIRE=" + PJO_COMMENTAIRE + ", PJO_NOM=" + PJO_NOM + ", PJO_BIN=" + PJO_BIN
					+ ", PJO_DATE_INSERTION=" + PJO_DATE_INSERTION + ", PJO_DATE_ENVOI_SI=" + PJO_DATE_ENVOI_SI
					+ ", PJO_GEDAFFAIRE=" + PJO_GEDAFFAIRE + ", PJO_GED_ENVOI_M03=" + PJO_GED_ENVOI_M03
					+ ", PJO_GED_ACTION=" + PJO_GED_ACTION + ", PJO_GED_DATE_ENVOI_M03=" + PJO_GED_DATE_ENVOI_M03
					+ ", PJO_DATE_RECEPTION_SI=" + PJO_DATE_RECEPTION_SI + ", PJO_MODE_ENVOI_SI=" + PJO_MODE_ENVOI_SI
					+ ", PJO_ETAT_ENVOI_GED_OPTIMUM=" + PJO_ETAT_ENVOI_GED_OPTIMUM + ", PJO_DATE_ENVOI_GED_OPTIMUM="
					+ PJO_DATE_ENVOI_GED_OPTIMUM + "]";
		}
		
		
		
		
		
}
