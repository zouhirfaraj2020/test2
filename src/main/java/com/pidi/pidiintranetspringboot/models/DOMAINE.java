package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Null;

@Entity
@Table(name="DOMAINE")
public class DOMAINE {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int DOM_ID; 
		@Null
		@Column(name = "DOM_CODE")
		private int DOM_CODE; 
		private String DOM_LIBELLE; 
		private Date DOM_DATE_CREATION; 
		private Date DOM_DATE_MODIFICATION;
		
		
		public int getDOM_CODE() {
			return DOM_CODE;
		}
		public void setDOM_CODE(int dOM_CODE) {
			DOM_CODE = dOM_CODE;
		}
		public String getDOM_LIBELLE() {
			return DOM_LIBELLE;
		}
		public void setDOM_LIBELLE(String dOM_LIBELLE) {
			DOM_LIBELLE = dOM_LIBELLE;
		}
		public Date getDOM_DATE_CREATION() {
			return DOM_DATE_CREATION;
		}
		public void setDOM_DATE_CREATION(Date dOM_DATE_CREATION) {
			DOM_DATE_CREATION = dOM_DATE_CREATION;
		}
		public Date getDOM_DATE_MODIFICATION() {
			return DOM_DATE_MODIFICATION;
		}
		public void setDOM_DATE_MODIFICATION(Date dOM_DATE_MODIFICATION) {
			DOM_DATE_MODIFICATION = dOM_DATE_MODIFICATION;
		}
		public DOMAINE() {
			super();
			// TODO Auto-generated constructor stub
		}
		public DOMAINE(int dOM_CODE, String dOM_LIBELLE, Date dOM_DATE_CREATION, Date dOM_DATE_MODIFICATION) {
			super();
			DOM_CODE = dOM_CODE;
			DOM_LIBELLE = dOM_LIBELLE;
			DOM_DATE_CREATION = dOM_DATE_CREATION;
			DOM_DATE_MODIFICATION = dOM_DATE_MODIFICATION;
		} 
		
		
}
