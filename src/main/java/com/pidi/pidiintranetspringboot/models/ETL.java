package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Null;

@Entity
@Table(name="ETL")
public class ETL {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int ETL_ID; 
		private int ETL_CODE; 
		private Date ETL_DATE_CREATION;
		private String ETL_LIBELLE;		
		//@NotNull
		@Null
	    @Column(name = "ETL_MAIL")
	    private String ETL_MAIL;
		private Date ETL_DATE_MODIFICATION;
		private String ETL_ADRESSE;
	    private String ETL_FAX;
		@Null
	    @Column(name = "ETL_MODE_TRANSMISSION")
	    private int ETL_MODE_TRANSMISSION;
		@Null
	    @Column(name = "ETL_DELAI_ENVOI")
		private int ETL_DELAI_ENVOI;
		private Date ETL_MEP_CERTIF;
		private Date ETL_FIN_CERTIF;
		@Null
	    @Column(name = "ETL_COMMUNICATION_LOGS")
		private int ETL_COMMUNICATION_LOGS;
		@Null
	    @Column(name = "ETL_ENVOI_ETECH_ACTIF")
		private int ETL_ENVOI_ETECH_ACTIF;
		@Null
	    @Column(name = "ETL_ENVOI_MAIL_REJET")
		private int ETL_ENVOI_MAIL_REJET;
		private String ETL_MAIL2;
		private String ETL_MAIL3;
		@Null
	    @Column(name = "ETL_SEUIL_APPEL_TAM")
		private int ETL_SEUIL_APPEL_TAM;
		@Null
	    @Column(name = "ETL_RESTRICTION_TAM")
		private int ETL_RESTRICTION_TAM;
		//@NotNull
		@Null
	    @Column(name = "ETL_DEBRAYAGE_CONTROLE_ESSAI")
		private int ETL_DEBRAYAGE_CONTROLE_ESSAI;
		private String ETL_URL_MPP;
		@Null
	    @Column(name = "ETL_ENVOI_REFERENTIEL")
		private int ETL_ENVOI_REFERENTIEL;
		@Null
	    @Column(name = "ETL_ENVOI_REFERENTIEL_VQSE")
		private int ETL_ENVOI_REFERENTIEL_VQSE;

		@ManyToOne
		private OT ot;

		
		public ETL() {
			super();
			// TODO Auto-generated constructor stub
		}


		public ETL(int eTL_ID, int eTL_CODE, Date eTL_DATE_CREATION, String eTL_LIBELLE, @Null String eTL_MAIL,
				Date eTL_DATE_MODIFICATION, String eTL_ADRESSE, String eTL_FAX, @Null int eTL_MODE_TRANSMISSION,
				@Null int eTL_DELAI_ENVOI, Date eTL_MEP_CERTIF, Date eTL_FIN_CERTIF, @Null int eTL_COMMUNICATION_LOGS,
				@Null int eTL_ENVOI_ETECH_ACTIF, @Null int eTL_ENVOI_MAIL_REJET, String eTL_MAIL2, String eTL_MAIL3,
				@Null int eTL_SEUIL_APPEL_TAM, @Null int eTL_RESTRICTION_TAM, @Null int eTL_DEBRAYAGE_CONTROLE_ESSAI,
				String eTL_URL_MPP, @Null int eTL_ENVOI_REFERENTIEL, @Null int eTL_ENVOI_REFERENTIEL_VQSE, OT ot) {
			super();
			ETL_ID = eTL_ID;
			ETL_CODE = eTL_CODE;
			ETL_DATE_CREATION = eTL_DATE_CREATION;
			ETL_LIBELLE = eTL_LIBELLE;
			ETL_MAIL = eTL_MAIL;
			ETL_DATE_MODIFICATION = eTL_DATE_MODIFICATION;
			ETL_ADRESSE = eTL_ADRESSE;
			ETL_FAX = eTL_FAX;
			ETL_MODE_TRANSMISSION = eTL_MODE_TRANSMISSION;
			ETL_DELAI_ENVOI = eTL_DELAI_ENVOI;
			ETL_MEP_CERTIF = eTL_MEP_CERTIF;
			ETL_FIN_CERTIF = eTL_FIN_CERTIF;
			ETL_COMMUNICATION_LOGS = eTL_COMMUNICATION_LOGS;
			ETL_ENVOI_ETECH_ACTIF = eTL_ENVOI_ETECH_ACTIF;
			ETL_ENVOI_MAIL_REJET = eTL_ENVOI_MAIL_REJET;
			ETL_MAIL2 = eTL_MAIL2;
			ETL_MAIL3 = eTL_MAIL3;
			ETL_SEUIL_APPEL_TAM = eTL_SEUIL_APPEL_TAM;
			ETL_RESTRICTION_TAM = eTL_RESTRICTION_TAM;
			ETL_DEBRAYAGE_CONTROLE_ESSAI = eTL_DEBRAYAGE_CONTROLE_ESSAI;
			ETL_URL_MPP = eTL_URL_MPP;
			ETL_ENVOI_REFERENTIEL = eTL_ENVOI_REFERENTIEL;
			ETL_ENVOI_REFERENTIEL_VQSE = eTL_ENVOI_REFERENTIEL_VQSE;
			this.ot = ot;
		}


		public int getETL_ID() {
			return ETL_ID;
		}


		public void setETL_ID(int eTL_ID) {
			ETL_ID = eTL_ID;
		}


		public int getETL_CODE() {
			return ETL_CODE;
		}


		public void setETL_CODE(int eTL_CODE) {
			ETL_CODE = eTL_CODE;
		}


		public Date getETL_DATE_CREATION() {
			return ETL_DATE_CREATION;
		}


		public void setETL_DATE_CREATION(Date eTL_DATE_CREATION) {
			ETL_DATE_CREATION = eTL_DATE_CREATION;
		}


		public String getETL_LIBELLE() {
			return ETL_LIBELLE;
		}


		public void setETL_LIBELLE(String eTL_LIBELLE) {
			ETL_LIBELLE = eTL_LIBELLE;
		}


		public String getETL_MAIL() {
			return ETL_MAIL;
		}


		public void setETL_MAIL(String eTL_MAIL) {
			ETL_MAIL = eTL_MAIL;
		}


		public Date getETL_DATE_MODIFICATION() {
			return ETL_DATE_MODIFICATION;
		}


		public void setETL_DATE_MODIFICATION(Date eTL_DATE_MODIFICATION) {
			ETL_DATE_MODIFICATION = eTL_DATE_MODIFICATION;
		}


		public String getETL_ADRESSE() {
			return ETL_ADRESSE;
		}


		public void setETL_ADRESSE(String eTL_ADRESSE) {
			ETL_ADRESSE = eTL_ADRESSE;
		}


		public String getETL_FAX() {
			return ETL_FAX;
		}


		public void setETL_FAX(String eTL_FAX) {
			ETL_FAX = eTL_FAX;
		}


		public int getETL_MODE_TRANSMISSION() {
			return ETL_MODE_TRANSMISSION;
		}


		public void setETL_MODE_TRANSMISSION(int eTL_MODE_TRANSMISSION) {
			ETL_MODE_TRANSMISSION = eTL_MODE_TRANSMISSION;
		}


		public int getETL_DELAI_ENVOI() {
			return ETL_DELAI_ENVOI;
		}


		public void setETL_DELAI_ENVOI(int eTL_DELAI_ENVOI) {
			ETL_DELAI_ENVOI = eTL_DELAI_ENVOI;
		}


		public Date getETL_MEP_CERTIF() {
			return ETL_MEP_CERTIF;
		}


		public void setETL_MEP_CERTIF(Date eTL_MEP_CERTIF) {
			ETL_MEP_CERTIF = eTL_MEP_CERTIF;
		}


		public Date getETL_FIN_CERTIF() {
			return ETL_FIN_CERTIF;
		}


		public void setETL_FIN_CERTIF(Date eTL_FIN_CERTIF) {
			ETL_FIN_CERTIF = eTL_FIN_CERTIF;
		}


		public int getETL_COMMUNICATION_LOGS() {
			return ETL_COMMUNICATION_LOGS;
		}


		public void setETL_COMMUNICATION_LOGS(int eTL_COMMUNICATION_LOGS) {
			ETL_COMMUNICATION_LOGS = eTL_COMMUNICATION_LOGS;
		}


		public int getETL_ENVOI_ETECH_ACTIF() {
			return ETL_ENVOI_ETECH_ACTIF;
		}


		public void setETL_ENVOI_ETECH_ACTIF(int eTL_ENVOI_ETECH_ACTIF) {
			ETL_ENVOI_ETECH_ACTIF = eTL_ENVOI_ETECH_ACTIF;
		}


		public int getETL_ENVOI_MAIL_REJET() {
			return ETL_ENVOI_MAIL_REJET;
		}


		public void setETL_ENVOI_MAIL_REJET(int eTL_ENVOI_MAIL_REJET) {
			ETL_ENVOI_MAIL_REJET = eTL_ENVOI_MAIL_REJET;
		}


		public String getETL_MAIL2() {
			return ETL_MAIL2;
		}


		public void setETL_MAIL2(String eTL_MAIL2) {
			ETL_MAIL2 = eTL_MAIL2;
		}


		public String getETL_MAIL3() {
			return ETL_MAIL3;
		}


		public void setETL_MAIL3(String eTL_MAIL3) {
			ETL_MAIL3 = eTL_MAIL3;
		}


		public int getETL_SEUIL_APPEL_TAM() {
			return ETL_SEUIL_APPEL_TAM;
		}


		public void setETL_SEUIL_APPEL_TAM(int eTL_SEUIL_APPEL_TAM) {
			ETL_SEUIL_APPEL_TAM = eTL_SEUIL_APPEL_TAM;
		}


		public int getETL_RESTRICTION_TAM() {
			return ETL_RESTRICTION_TAM;
		}


		public void setETL_RESTRICTION_TAM(int eTL_RESTRICTION_TAM) {
			ETL_RESTRICTION_TAM = eTL_RESTRICTION_TAM;
		}


		public int getETL_DEBRAYAGE_CONTROLE_ESSAI() {
			return ETL_DEBRAYAGE_CONTROLE_ESSAI;
		}


		public void setETL_DEBRAYAGE_CONTROLE_ESSAI(int eTL_DEBRAYAGE_CONTROLE_ESSAI) {
			ETL_DEBRAYAGE_CONTROLE_ESSAI = eTL_DEBRAYAGE_CONTROLE_ESSAI;
		}


		public String getETL_URL_MPP() {
			return ETL_URL_MPP;
		}


		public void setETL_URL_MPP(String eTL_URL_MPP) {
			ETL_URL_MPP = eTL_URL_MPP;
		}


		public int getETL_ENVOI_REFERENTIEL() {
			return ETL_ENVOI_REFERENTIEL;
		}


		public void setETL_ENVOI_REFERENTIEL(int eTL_ENVOI_REFERENTIEL) {
			ETL_ENVOI_REFERENTIEL = eTL_ENVOI_REFERENTIEL;
		}


		public int getETL_ENVOI_REFERENTIEL_VQSE() {
			return ETL_ENVOI_REFERENTIEL_VQSE;
		}


		public void setETL_ENVOI_REFERENTIEL_VQSE(int eTL_ENVOI_REFERENTIEL_VQSE) {
			ETL_ENVOI_REFERENTIEL_VQSE = eTL_ENVOI_REFERENTIEL_VQSE;
		}


		public OT getOt() {
			return ot;
		}


		public void setOt(OT ot) {
			this.ot = ot;
		} 
		
		
		
}
