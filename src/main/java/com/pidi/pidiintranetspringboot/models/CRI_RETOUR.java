package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CRI_RETOUR {

      		@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)
			private int RET_CODE;
			private int CRI_ID; 
			private Date ERR_DATE;
			public CRI_RETOUR(int rET_CODE, int cRI_ID, Date eRR_DATE) {
				super();
				RET_CODE = rET_CODE;
				CRI_ID = cRI_ID;
				ERR_DATE = eRR_DATE;
			}
			
			public CRI_RETOUR() {
				super();
			}

			public int getRET_CODE() {
				return RET_CODE;
			}

			public void setRET_CODE(int rET_CODE) {
				RET_CODE = rET_CODE;
			}

			public int getCRI_ID() {
				return CRI_ID;
			}

			public void setCRI_ID(int cRI_ID) {
				CRI_ID = cRI_ID;
			}

			public Date getERR_DATE() {
				return ERR_DATE;
			}

			public void setERR_DATE(Date eRR_DATE) {
				ERR_DATE = eRR_DATE;
			}

			@Override
			public String toString() {
				return "CRI_RETOUR [RET_CODE=" + RET_CODE + ", CRI_ID=" + CRI_ID + ", ERR_DATE=" + ERR_DATE + "]";
			}
			
}
