package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ND")
public class ND {

	

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int OT_ID; 
		private int ND;
		public int getOT_ID() {
			return OT_ID;
		}
		public void setOT_ID(int oT_ID) {
			OT_ID = oT_ID;
		}
		public int getND() {
			return ND;
		}
		public void setND(int nD) {
			ND = nD;
		}
		public ND() {
			super();
			// TODO Auto-generated constructor stub
		}
		public ND(int oT_ID, int nD) {
			super();
			OT_ID = oT_ID;
			ND = nD;
		}
		
		
		
		
		
}
