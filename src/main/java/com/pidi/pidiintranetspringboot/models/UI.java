package com.pidi.pidiintranetspringboot.models;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Null;

@Entity
@Table(name="UI")
public class UI {

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int UI_ID; 
		private int ENT_ID; 
		private String UI_CODE;
		private String UI_LIBELLE;
		
		@Null
		@Column(name = "ETL_ID")
	    private int ETL_ID;
		//@NotNull
		@Null
		@Column(name = "UI_TYPE")
		private int UI_TYPE;
		@Null
		@Column(name = "UI_BL")
		private int UI_BL;
		@Null
		@Column(name = "UI_DELAI")
		private int UI_DELAI;
		private Date UI_DATE_CREATION;
	    private Date UI_DATE_MODIFICATION;
		@Null
		@Column(name = "UI_DELAI_GDP")
		private int UI_DELAI_GDP;
		
		@ManyToOne
		private OT ot;
		
		@OneToMany
	    private Set<ENTITE> entite = new HashSet<ENTITE>(0);
		
		
		public UI() {
			super();
			// TODO Auto-generated constructor stub
		}


		public UI(int uI_ID, int eNT_ID, String uI_CODE, String uI_LIBELLE, @Null int eTL_ID, @Null int uI_TYPE,
				@Null int uI_BL, @Null int uI_DELAI, Date uI_DATE_CREATION, Date uI_DATE_MODIFICATION,
				@Null int uI_DELAI_GDP, OT ot, Set<ENTITE> entite) {
			super();
			UI_ID = uI_ID;
			ENT_ID = eNT_ID;
			UI_CODE = uI_CODE;
			UI_LIBELLE = uI_LIBELLE;
			ETL_ID = eTL_ID;
			UI_TYPE = uI_TYPE;
			UI_BL = uI_BL;
			UI_DELAI = uI_DELAI;
			UI_DATE_CREATION = uI_DATE_CREATION;
			UI_DATE_MODIFICATION = uI_DATE_MODIFICATION;
			UI_DELAI_GDP = uI_DELAI_GDP;
			this.ot = ot;
			this.entite = entite;
		}


		public int getUI_ID() {
			return UI_ID;
		}


		public void setUI_ID(int uI_ID) {
			UI_ID = uI_ID;
		}


		public int getENT_ID() {
			return ENT_ID;
		}


		public void setENT_ID(int eNT_ID) {
			ENT_ID = eNT_ID;
		}


		public String getUI_CODE() {
			return UI_CODE;
		}


		public void setUI_CODE(String uI_CODE) {
			UI_CODE = uI_CODE;
		}


		public String getUI_LIBELLE() {
			return UI_LIBELLE;
		}


		public void setUI_LIBELLE(String uI_LIBELLE) {
			UI_LIBELLE = uI_LIBELLE;
		}


		public int getETL_ID() {
			return ETL_ID;
		}


		public void setETL_ID(int eTL_ID) {
			ETL_ID = eTL_ID;
		}


		public int getUI_TYPE() {
			return UI_TYPE;
		}


		public void setUI_TYPE(int uI_TYPE) {
			UI_TYPE = uI_TYPE;
		}


		public int getUI_BL() {
			return UI_BL;
		}


		public void setUI_BL(int uI_BL) {
			UI_BL = uI_BL;
		}


		public int getUI_DELAI() {
			return UI_DELAI;
		}


		public void setUI_DELAI(int uI_DELAI) {
			UI_DELAI = uI_DELAI;
		}


		public Date getUI_DATE_CREATION() {
			return UI_DATE_CREATION;
		}


		public void setUI_DATE_CREATION(Date uI_DATE_CREATION) {
			UI_DATE_CREATION = uI_DATE_CREATION;
		}


		public Date getUI_DATE_MODIFICATION() {
			return UI_DATE_MODIFICATION;
		}


		public void setUI_DATE_MODIFICATION(Date uI_DATE_MODIFICATION) {
			UI_DATE_MODIFICATION = uI_DATE_MODIFICATION;
		}


		public int getUI_DELAI_GDP() {
			return UI_DELAI_GDP;
		}


		public void setUI_DELAI_GDP(int uI_DELAI_GDP) {
			UI_DELAI_GDP = uI_DELAI_GDP;
		}


		public OT getOt() {
			return ot;
		}


		public void setOt(OT ot) {
			this.ot = ot;
		}


		public Set<ENTITE> getEntite() {
			return entite;
		}


		public void setEntite(Set<ENTITE> entite) {
			this.entite = entite;
		}
		
		
		
}
