package com.pidi.pidiintranetspringboot.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ETAT_INTERVENTION")
public class ETAT_INTERVENTION {


			@Id
			@GeneratedValue(strategy = GenerationType.IDENTITY)
			private int ETA_ID; 
			private String ETA_LIBELLE;
			public ETAT_INTERVENTION() {
				super();
				// TODO Auto-generated constructor stub
			}
			public ETAT_INTERVENTION(int eTA_ID, String eTA_LIBELLE) {
				super();
				ETA_ID = eTA_ID;
				ETA_LIBELLE = eTA_LIBELLE;
			}
			public int getETA_ID() {
				return ETA_ID;
			}
			public void setETA_ID(int eTA_ID) {
				ETA_ID = eTA_ID;
			}
			public String getETL_LIBELLE() {
				return ETA_LIBELLE;
			}
			public void setETL_LIBELLE(String eTA_LIBELLE) {
				ETA_LIBELLE = eTA_LIBELLE;
			} 
			
			
}
