package com.pidi.pidiintranetspringboot.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.pidi.pidiintranetspringboot.exception.FileStorageException;
import com.pidi.pidiintranetspringboot.exception.MyFileNotFoundException;
import com.pidi.pidiintranetspringboot.models.DBFile;
import com.pidi.pidiintranetspringboot.models.Test1;
import com.pidi.pidiintranetspringboot.repository.DBFileRepository;
import com.pidi.pidiintranetspringboot.repository.OtRepo;
import com.pidi.pidiintranetspringboot.repository.RepoTest1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Service
public class DBFileStorageService {
    
    @Autowired
	private RepoTest1 test1Repo;
    
  @Autowired
    private DBFileRepository dbFileRepository;

    public DBFile storeFile(MultipartFile file) {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            DBFile dbFile = new DBFile(fileName, file.getContentType(), file.getBytes());

            return dbFileRepository.save(dbFile);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    public DBFile getFile(String fileId) {
        return dbFileRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }
    
    String line="";
    
    /*public void saveDb()
    {
    	try {
    		BufferedReader br=new BufferedReader(new FileReader("src/main/resources/test.csv"));
    		while((line=br.readLine())!=null)
    		{
    			String[] data=line.split(",");
    			Test1 t = new Test1();
    			t.setNom(data[0]);
    			t.setPrenom(data[1]);
    			test1Repo.save(t);
    		}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    }*/
}